# -*- coding: utf-8 -*-
# title     : 1-create_samples.py
# author    : WD41, LRS/EPFL/PSI
# date      : February 2015
# purpose   : Python driver scripts to generate Sobol samples for FEBA TRACE
# usage     : > python 1-create_samples.py
# py_version: 3.4.2 (Anaconda Python Distribution 2.1.0 (64bit),
#           : https://store.continuum.io/cshop/anaconda
###############################################################################

def main():
    
    import numpy as np

    from sa_module import sobol
    from sa_module import trace

    import common_data_sobol as common_data

    case_no = common_data.cases[1]
    ####################################
    # Set parameters for Sobol samples #
    ####################################
    num_samples = 2000
    num_params = 10
    scheme = "sobol"
    sobol_params = [
        "./apps_and_libs/sobol.o", 
        "./apps_and_libs/new-joe-kuo-6.21201"] 

    ####################################################
    # Generate Sobol samples and save them into a file #
    ####################################################
    sobol_sample = sobol.sobol_saltelli.create(
        num_samples, num_params, sobol_params, scheme
    )
    filename = "feba-sobol_{}_{}_{}.txt" .format(
        num_params, num_samples, scheme
    )
    fullname = "{}/{}" .format(common_data.setf_dir, filename)
    sobol.sobol_saltelli.write(sobol_sample, fullname)

    #############################
    # read listVars input files #
    #############################
    vars_inp_filename = "{}/febaVars{}Screened.inp" .format(
        common_data.base_dir, case_no
    )
    vars_dict = trace.list_to_dict.inpvars_to_dict(vars_inp_filename)
    trace.create_keys.create_keys(vars_dict, keyword="$")

    #################################################
    # Rescale Sobol-Saltelli Design and write it    #
    #################################################

    # Create and Write Morris Scaled Design Matrix
    sobol_sample_rescaled = trace.list_to_dict.rescale_design(
        vars_dict, fullname
    )
    sample_filename = "feba-sobol_{}_{}_{}_{}.txt" .format(
        case_no,
        num_params, 
        num_samples,
        scheme
    )
    sample_fullname = "{}/{}" .format(common_data.case_dir, sample_filename)
    
    np.savetxt(sample_fullname, sobol_sample_rescaled, fmt="%1.6e")


if __name__ == "__main__":
    main()

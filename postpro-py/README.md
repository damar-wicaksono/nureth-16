# postpro-py

Collection of python3 scripts to produce postprocessed data used in making all
the plots. The scripts are the following:

 - `get_sobol_indices_tempmax.py`: compute Sobol' indices for the 10-parameter
   model using Janon et al. estimator and maximum temperature as the QoI
 - `get_sobol_indices_tquench.py`: compute Sobol' indices for the 10-parameter
   model using Janon et al. estimator and time of quenching as the QoI
 - `get_sobol_indices_running.py`: Compute the running estimate of Sobol' main 
   sensitivity indices using two different estimators for the 10-parameter 
   model using maximum temperature as the QoI
 - `get_sobol_indices_evolution.py`: Compute the sensitivity indices for each
   time step of the temperature output
 - `get_sobol_indices_fpc1.py`: compute Sobol' indices for the 10-parameter
   model using Saltelli et al. estimator and 1st fPC score as the QoI
 - `get_sobol_indices_fpc2.py`: compute Sobol' indices for the 10-parameter
   model using Saltelli et al. estimator and 2nd fPC score as the QoI
 - `get_sobol_indices_conv_fpc1.py`: Compute the bootstraps samples of Sobol 
    estimates using 3 different sample sizes with 1st fPC score as the QoI
 - `get_sobol_indices_conv_tempmax.py`: Compute the bootstraps samples of Sobol 
    estimates using 3 different sample sizes with max. temperature as the QoI
	
The scripts required Python3 to be installed (Anaconda Python v3.5 is recommended).
All the scripts can be executed with the following command:

	python <name of the script>
	
The prerequisite and the outputs for each of the scripts can be found in the 
corresponding header comments.

a `Makefile` was prepared to shortcut the post-processing. Simply run,

    make all

To run all the scripts

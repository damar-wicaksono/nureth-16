#!/usr/bin/env python
# title         : get_sobol_indices_conv_tempmax.py
# description   : Compute the bootstraps samples of Sobol estimates using 3
#               : different sample sizes with maximum temperature as the QoI
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_sobol_indices_conv_tempmax.py
# prerequisite  : postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat
# output        : 1) tempmax-bootstrap-250-janon.csv
#               : 2) tempmax-bootstrap-500-janon.csv
#               : 3) tempmax-bootstrap-1000-janon.csv
#               : 4) tempmax-bootstrap-250-saltelli.csv
#               : 5) tempmax-bootstrap-500-saltelli.csv
#               : 6) tempmax-bootstrap-100-saltelli.csv
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common

def main():
    # Read data
    param_names = common.param_names
    k = 10
    tempmax_mid = np.loadtxt("../postpro-sim/216-sobol-screened/postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat")
    tempmax = np.empty([0])
    for j in range(2, 2000*(k+2)+2):
        tempmax = np.append(tempmax, np.max(tempmax_mid[:, j]))
    for i in [250, 500, 1000]:
        tempmax_dict = sobol.sobol_saltelli.read(tempmax, i, k)
        tempmax_indices_saltelli, tempmax_bootstrap_saltelli = sobol.indices.evaluate_1st(tempmax_dict, True, 20151418, 10000)
        tempmax_indices_janon, tempmax_bootstrap_janon = sobol.indices.evaluate_1st_janon(tempmax_dict, True, 20151418, 10000)
        # Save into files
        common.save_bootstrap("./tempmax-bootstrap-{}-saltelli.csv" .format(i), 
                              tempmax_bootstrap_saltelli, param_names, 10000)
        common.save_bootstrap("./tempmax-bootstrap-{}-janon.csv" .format(i), 
                              tempmax_bootstrap_janon, param_names, 10000)


if __name__ == "__main__":
    main()

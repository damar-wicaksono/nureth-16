#!/usr/bin/env python
# title         : get_sobol_indices_fpc2.py
# description   : Compute the running estimate of Sobol' main sensitivity 
#               : indices using Saltelli et al. estimators for the 10-parameter 
#               : model using 2nd fpc score as the QoI 
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_sobol_indices_fpc1.py
# prerequisite  : sobol-tc-mid-pca-scores.txt
# output        : 1) fpc2-indices-2000-saltelli.txt
#               : 2) fpc2-bootstrap-2000-janon.txt
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def main():
    # Read the data
    param_names = common.param_names
    reg_pc_mid = np.loadtxt("../postpro-R/mid-2000/sobol-tc-mid-pca-scores.txt", 
                            skiprows=1, delimiter=",", usecols=range(1,5))
    k = 10
    pc2 = np.empty([0])
    for j in range(2000*(k+2)):
        pc2 = np.append(pc2, reg_pc_mid[j, 1])
    # Compute the sensitivity indices
    pc2_dict = sobol.sobol_saltelli.read(pc2, 2000, k)
    pc2_indices, pc2_bootstrap = sobol.indices.evaluate_1st(pc2_dict, 
                                                            True, 
                                                            20151418, 10000)
    # Save into files
    with open("./fpc2-bootstrap-2000-saltelli.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": pc2_bootstrap[key][j], 
                                 "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": pc2_bootstrap[key][j], 
                                 "parameter": param_names[i], "index": "total"})

    with open("./fpc2-indices-2000-saltelli.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            writer.writerow({"value": pc2_indices[key], 
                             "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            writer.writerow({"value": pc2_indices[key], 
                             "parameter": param_names[i], "index": "total"})


if __name__ == "__main__":
    main()
#!/usr/bin/env python
# title         : get_sobol_indices_evolution.py
# description   : Compute the evolution of the Sobol' indices for the 
#               : time-dependent temperature at mid-assembly output as the QoI
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_running_indices_running.py
# prerequisite  : sobol-tc-mid-reg-2000-*.csv (* for * in range(1,11))
# output        : si-evol.txt
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def main():
    # Read Registered Curves
    reg_tc_mid = np.loadtxt("../postpro-R/mid-2000/sobol-tc-mid-reg-2000-1.csv", 
                            skiprows=0, delimiter=",")
    for i in range(2, 11):
        infilename = "../postpro-R/mid-2000/sobol-tc-mid-reg-2000-{}.csv" .format(i)
        reg_tc_mid = np.column_stack((reg_tc_mid, np.loadtxt(infilename, 
                                      skiprows=0, delimiter=",", 
                                      usecols=range(1,2401))))

    # Loop over time, t = 0 [s]
    k = 10
    si = []
    si.append(reg_tc_mid[0, 0])
    t_evol = np.empty([0])
    for j in range(1, 2000*(k+2)+1):
        t_evol = np.append(t_evol, reg_tc_mid[0, j])
    t_evol_dict = sobol.sobol_saltelli.read(t_evol, 2000, k)
    t_evol_indices = sobol.indices.evaluate_1st_janon(t_evol_dict, bootstrap=False)
    for k in range(1, 11):
        key = "S{}" .format(k)
        si.append(t_evol_indices[key])
    for k in range(1, 11):
        key = "ST{}" .format(k)
        si.append(t_evol_indices[key])
    si_array = np.array(si)
    # Loop over time, the rest
    for i in range(1, reg_tc_mid.shape[0]):
        si = []
        si.append(reg_tc_mid[i, 0])
        t_evol = np.empty([0])
        for j in range(1, 2000*(k+2)+1):
            t_evol = np.append(t_evol, reg_tc_mid[i, j])
        t_evol_dict = sobol.sobol_saltelli.read(t_evol, 2000, k)
        t_evol_indices = sobol.indices.evaluate_1st_janon(t_evol_dict, bootstrap=False)
        for k in range(1, 11):
            key = "S{}" .format(k)
            si.append(t_evol_indices[key])
        for k in range(1, 11):
            key = "ST{}" .format(k)
            si.append(t_evol_indices[key])
        si_array = np.vstack((si_array, np.array(si)))

    # Save into a file
    np.savetxt("si_evol.txt", si_array, fmt="%1.3e")


if __name__ == "__main__":
    main()

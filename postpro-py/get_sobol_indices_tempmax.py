#!/usr/bin/env python
# title         : get_sobol_indices_tempmax.py
# description   : Compute the Sobol' sensitivity indices (main and total)
#               : for the 10-parameter model using maximum temperature as the 
#               : QoI and the Janon et al. estimator
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_sobol_indices_tempmax.py
# prerequisite  : postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat
# output        : 1) tempmax-indices-2000-janon.csv
#               : 2) tempmax-bootstrap-2000-janon.csv
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def main():
    # Read the required data
    param_names = common.param_names
    tempmax_mid = np.loadtxt("../postpro-sim/216-sobol-screened/postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat")
    k = 10
    tempmax = np.empty([0])
    for j in range(2, 2000*(k+2)+2):
        tempmax = np.append(tempmax, np.max(tempmax_mid[:, j]))
    
    # Calculate the Sobol Indices
    tempmax_dict = sobol.sobol_saltelli.read(tempmax, 200, k)
    tempmax_indices, tempmax_bootstrap = sobol.indices.evaluate_1st_janon(
        tempmax_dict, True, 20151418, 10000)
    
    # Write the results into files
    with open("./tempmax-bootstrap-2000-janon.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": tempmax_bootstrap[key][j], 
                                 "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": tempmax_bootstrap[key][j], 
                                 "parameter": param_names[i], 
                                 "index": "total"})

    with open("./tempmax-indices-2000-janon.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            writer.writerow({"value": tempmax_indices[key], 
                             "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            writer.writerow({"value": tempmax_indices[key], 
                             "parameter": param_names[i], "index": "total"})


if __name__ == "__main__":
    main()

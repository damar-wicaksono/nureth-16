param_names = {1 : "break_ptb", 
               2 : "fill_vmtbm",
               3 : "power_rpwtbr",
               4 : "gridHTEnh",
               5 : "iafbWallHTC",
               6 : "dffbWallHTC",
               7 : "dffbVIHTC",
               8 : "iafbIntDrag",
               9 : "dffbIntDrag",
               10 : "tempQuench"}


def save_bootstrap(outfilename, bootstrap_dict, param_names, bootstrap_samples):
    import csv
    with open(outfilename, "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            for j in range(bootstrap_samples):
                writer.writerow({"value": bootstrap_dict[key][j], 
                                 "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            for j in range(bootstrap_samples):
                writer.writerow({"value": bootstrap_dict[key][j], 
                                 "parameter": param_names[i], "index": "total"})
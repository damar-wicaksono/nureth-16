#!/usr/bin/env python
# title         : get_sobol_indices_tquench.py
# description   : Compute the Sobol' sensitivity indices (main and total)
#               : for the 10-parameter model using time of quenching as the QoI 
#               : and the Janon et al. estimator
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_running_indices_tquench.py
# prerequisite  : sobol-tc-mid-landmarks.txt
# output        : 1) tquench-indices-2000-janon.csv
#               : 2) tquench-bootstrap-2000-janon.csv
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def main():
    param_names = common.param_names
    tquench_mid = np.loadtxt("../postpro-R/mid-2000/sobol-tc-mid-landmarks.txt", 
                             skiprows=2, 
                             delimiter=",", 
                             usecols=range(2,3))
    k=10
    tquench = np.empty([0])
    for j in range(0, 2000*(k+2)):
        tquench = np.append(tquench, tquench_mid[j])
    tquench_dict = sobol.sobol_saltelli.read(tquench, 2000, k)
    tquench_indices, tquench_bootstrap = sobol.indices.evaluate_1st_janon(
        tquench_dict, True, 20151418, 10000)

    # Write the results into files
    with open("./tquench-bootstrap-2000-janon.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": tquench_bootstrap[key][j], 
                                 "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            for j in range(10000):
                writer.writerow({"value": tquench_bootstrap[key][j], 
                                 "parameter": param_names[i], 
                                 "index": "total"})

    with open("./tquench-indices-2000-janon.csv", "w") as outfile:
        fieldnames = ["value", "parameter", "index"]
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
    
        writer.writeheader()
        for i in range(1, 11):
            key = "S{}" .format(i)
            writer.writerow({"value": tquench_indices[key], 
                             "parameter": param_names[i], "index": "main"})
            key = "ST{}" .format(i)
            writer.writerow({"value": tquench_indices[key], 
                             "parameter": param_names[i], "index": "total"})


if __name__ == "__main__":
    main()

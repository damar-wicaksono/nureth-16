#!/usr/bin/env python
# title         : get_sobol_indices_running.py
# description   : Compute the running estimate of Sobol' main sensitivity 
#               : indices using two different estimators for the 10-parameter 
#               : model using maximum temperature as the QoI 
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_running_indices_running.py
# prerequisite  : postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat
# output        : 1) tempmax-running_indices-saltelli.txt
#               : 2) tempmax-running_indices-janon.txt
# py_version    : 3.5
#

import os
import csv
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def running_indices(qoi, k, n, estimator):
    """Calculate the running indices"""
    runs = [_ for _ in range(2, n+1)]
    running_indices = []
    for run in runs:
        temp = []
        y_dict = sobol.sobol_saltelli.read(qoi[0:(run*(k+2))], run, k)
        if estimator == "saltelli":
            y_indices = sobol.indices.evaluate_1st(y_dict, bootstrap=False)
        elif estimator == "janon":
            y_indices = sobol.indices.evaluate_1st_janon(y_dict, bootstrap=False)
        else:
            break
        for i in range(1, k+1):
            key = "S{}" .format(i)
            temp.append(y_indices[key])
        for i in range(1, k+1):
            key = "ST{}" .format(i)
            temp.append(y_indices[key])
        running_indices.append(temp)
    return np.array(running_indices)


def main():
    # Read the data
    k = 10
    tempmax_mid = np.loadtxt("../postpro-sim/216-sobol-screened/postpro-feba-216-10-sobol-sobol-2000-unif_mid.mat")
    tempmax = np.empty([0])
    for j in range(2, 2000*(k+2)+2):
        tempmax = np.append(tempmax, np.max(tempmax_mid[:, j]))

    # Compute the running indices estimates
    tempmax_running_indices = running_indices(tempmax, 10, 1000, "saltelli")
    np.savetxt("./tempmax-running_indices-saltelli.txt", 
               tempmax_running_indices, fmt="%.5f", delimiter=",")
    tempmax_running_indices = running_indices(tempmax, 10, 1000, "janon")
    np.savetxt("./tempmax-running_indices-janon.txt", 
               tempmax_running_indices, fmt="%.5f", delimiter=",")


if __name__ == "__main__":
    main()
#!/usr/bin/env python
# title         : get_sobol_indices_conv_fpc1.py
# description   : Compute the bootstraps samples of sobol estimates using 3
#               : different sample sizes with 1st fPC score as the QoI
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python get_sobol_indices_conv_fpc1.py
# prerequisite  : sobol-tc-mid-pca-scores.txt
# output        : 1) fpc1-bootstrap-250-janon.csv
#               : 2) fpc1-bootstrap-500-janon.csv
#               : 3) fpc1-bootstrap-1000-janon.csv
#               : 4) fpc1-bootstrap-250-saltelli.csv
#               : 5) fpc1-bootstrap-500-saltelli.csv
#               : 6) fpc1-bootstrap-100-saltelli.csv
# py_version    : 3.5
#

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
from sa_module import sobol
import common


def main():
    # Read the data
    param_names = common.param_names
    reg_pc_mid = np.loadtxt("../postpro-R/mid-2000/sobol-tc-mid-pca-scores.txt", 
                            skiprows=1, delimiter=",", usecols=range(1,5))
    k = 10
    pc1 = np.empty([0])
    for j in range(2000*(k+2)):
        pc1 = np.append(pc1, reg_pc_mid[j, 0])
    for i in [250, 500, 1000]:
        pc1_dict = sobol.sobol_saltelli.read(pc1, i, k)
        pc1_indices_saltelli, pc1_bootstrap_saltelli = sobol.indices.evaluate_1st(
            pc1_dict, True, 20151418, 10000)
        pc1_indices_janon, pc1_bootstrap_janon = sobol.indices.evaluate_1st_janon(
            pc1_dict, True, 20151418, 10000)
        # Save into files
        common.save_bootstrap("./fpc1-bootstrap-{}-saltelli.csv" .format(i), 
                              pc1_bootstrap_saltelli, param_names, 10000)
        common.save_bootstrap("./fpc1-bootstrap-{}-janon.csv" .format(i), 
                              pc1_bootstrap_janon, param_names, 10000)


if __name__ == "__main__":
    main()

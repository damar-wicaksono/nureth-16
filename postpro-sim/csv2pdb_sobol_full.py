# -*- coding: utf-8 -*-
# title     : csv2pdb_sobol_full.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2015
# purpose   : Python driver script to convert all the csv files into a single 
#           : pickled database file, for the Sobol' 26-parameter model campaign
# usage     : > python csv2pdb_sobol_full.py <sample_start> <sample_end>
# py_version: 3.3.4 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
#

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
import pandas as pd
import pickle

import common_data_sobol as common_data
from sa_module import sobol


def main():

    num_samples = 1000
    num_params = 26
    list_runs = sobol.sobol_saltelli.create_list_runs(num_samples, num_params)

    start = int(sys.argv[-2])
    end = int(sys.argv[-1])
    orig_data_run = []
    for i in range(start-1, end):
        print("Reading for Pickling, Sobol sample No. {}" .format(i+1))
        for run in list_runs[i]:
            run_fullname = "{}/febaTrans216_run_sobol_sobol_{}.csv" .format(common_data.postpro_case_dir_full, run)
            orig_data_run.append(np.loadtxt(run_fullname, delimiter=",", skiprows=2))
 
    pickle_filename = "{}/feba216-sobol-sobol-{}-{}.db" .format(common_data.postpro_case_dir_full,
                                                                start, end)
    with open(pickle_filename, "wb") as fileObject:
        pickle.dump(orig_data_run, fileObject)
            

if __name__ == "__main__":
    main()

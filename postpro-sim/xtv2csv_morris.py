# -*- coding: utf-8 -*-
# title     : xtv2csv_morris.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2015
# purpose   : Python driver script to convert xtv (or dmx) file into csv file
#           : for the Morris campaign
# usage     : > python xtv2csv_morris.py
# py_version: 3.3.4 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
#

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
import subprocess

import common_data_morris as common_data

from sa_module import runutils
from sa_module import trace

def main():

    case_no = common_data.case_no
    num_params = common_data.num_params
    num_trajectories = common_data.num_trajectories
    num_levels = common_data.num_levels

    ##############################
    # Common Directory and Files #
    ##############################
    workspace_dir = "/afs/psi.ch/project/stars/workspace/RND/"\
                    "SB-RND-ACT-006-13/WD41/projects/nureth-16"
    project_name = "simulation"
    apt_plot = "aptplot_v6.5.2_inst01.sh"

    workspace_dir_project = "{}/{}" .format(workspace_dir, project_name)
    base_dir = "{}/base" .format(workspace_dir_project)
    postpro_dir = common_data.postpro_case_dir 
    case_dir = "{}/{}-morris" .format(workspace_dir_project, case_no)

    #############################
    # read listVars input files #
    #############################
    vars_inp_filename = "{}/febaVars{}.inp" .format(base_dir, case_no)
    vars_dict = trace.list_to_dict.inpvars_to_dict(vars_inp_filename)

    # Construct a list of unique parameter names
    param_names = []
    for var in vars_dict:
        if var["type"] == "senscoef":
            param_names.append("{}_{}" .format(var["type"], var["num"]))
        else:
            param_names.append("{}_{}_{}" .format(var["type"],
                                                  var["num"],
                                                  var["var_name"]))

    param_names_short = []
    for i in range(len(param_names)):
        param_names_short.append("{}" .format(str(i+1)))
    total_runs = (int(num_params)+1) * int(num_trajectories)

    for i in range(1, total_runs+1):

        run_key = "run_{}_{}_{}" .format(num_trajectories, num_levels, str(i))
        run_dir = "{}/{}" .format(case_dir, run_key)
        run_filename = "febaTrans{}_{}" .format(case_no, run_key)

        # Convert to csv file
        select_vars = ["rftn-20A69R29", "rftn-20A89R29",
                       "rftn-20A109R29", "pn-30A04",
                       "pn-1A17R01T01", "pn-1A13R01T01",
                       "pn-1A01R01T01"]

        trace.trace_postpro.xtv_to_csv(
            apt_plot,
            run_filename,
            select_vars,
            case_no,
            i,
            xtv_ext="dmx",
            workdir=run_dir
        )

        csv_filename = "{}.csv" .format(run_filename)
        csv_fullname1 = "{}/{}" .format(run_dir, csv_filename)
        csv_fullname2 = "{}/{}" .format(postpro_dir, csv_filename)
        subprocess.call(["mv", csv_fullname1, csv_fullname2])

if __name__ == "__main__":
    main()

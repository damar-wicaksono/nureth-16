# -*- coding: utf-8 -*-
# title     : csv2pdb_morris.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2015
# purpose   : Python driver script to convert the csv files into a single 
#           : pickle database file, for the Morris campaign
# usage     : > python csv2pdb_morris.py
# py_version: 3.3.4 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
#

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
import pandas as pd
import pickle
import subprocess

import common_data_morris as common_data
from sa_module import trace


def main():

    key = "rftn-20A89R29"   # Mid-Assembly
    key = " {}" .format(key)
    max_t = 600.
    delta_t = 0.1
    num_trajectories = common_data.num_trajectories
    num_params = common_data.num_params
    num_levels = common_data.num_levels
    postpro_dir = common_data.postpro_case_dir

    total_runs = num_trajectories * (num_params + 1)

    unif_time = np.arange(0.0, max_t, delta_t)
        
    # Base case Runs
    run_fullname = "./216-base/febaTrans216_base.csv"
    df = pd.read_csv(run_fullname)
    df = df[1:]
    unif_trace_var = trace.unif_time.create(df, key, max_t, delta_t) 
    unif_data = np.column_stack((unif_time, unif_trace_var))
    orig_data = []

    for i in range(total_runs):
        print("Reading for Pickling Morris runs No. {}" .format(i+1))
        run_fullname = "{}/febaTrans216_run_{}_{}_{}.csv" .format(postpro_dir, 
                                                                  num_trajectories, 
                                                                  num_levels, 
                                                                  i+1)
        orig_data.append(np.loadtxt(run_fullname, delimiter=",", skiprows=2))
        df = pd.read_csv(run_fullname)
        # Remove the row containing unit from the dataframe
        df = df[1:]
        # The read_csv method give additional space in front of trace var
        unif_trace_var = trace.unif_time.create(df, key, max_t, delta_t) 
        # Add the resulting trace variable in uniform time grid to the 
        # original matrix
        unif_data = np.column_stack((unif_data, unif_trace_var))

    # The matrix file include the base case, while the pickled database doesn't
    np.savetxt("{}/postpro-feba-216-26-morris-40-unif_mid.mat" .format(postpro_dir), 
               unif_data, fmt="%1.4e")
    
    # Save the whole csv files into a Pickle binary with list of np.array
    pickle_filename = "{}/postpro-feba-216-26-morris-40.db" .format(postpro_dir) 
    with open(pickle_filename, "wb") as fileObject:
        pickle.dump(orig_data, fileObject)
        
    # Clean all the csv files
    for i in range(total_runs):
        print("Removing the csv file for runs No. {}" .format(i+1))
        run_fullname = "{}/febaTrans216_run_{}_{}_{}.csv" .format(postpro_dir, 
                                                                  num_trajectories, 
                                                                  num_levels, 
                                                                  i+1)
        subprocess.call(["rm", "-f", run_fullname])


if __name__ == "__main__":
    main()

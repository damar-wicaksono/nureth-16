# postpro-sim

A collection of python scripts to post-process the xtv output of TRACE runs
into a big single file (either ASCII or python pickled binary) for convenient
storage and downstream analysis. The post-processing is divided into three 
parts corresponding to the three simulation experiment campaigns: the Morris 
screening, the Sobol' indices for the 26-parameter FEBA TRACE model, and the
Sobol' indices for the 10-parameter FEBA TRACE model. Additionally, the base
case results is also extracted in the folder `216-base/`

Note that none of the `.xtv`, `.dmx` or `.csv` files is archived. As such, 
some of the scripts such as `xtv2csv` or `csv2pdb` would not work out of the 
box. For complete reproduction, all the runs have to be reproduced based on the 
corresponding design matrix file. However, several relevant outputs have been
extracted and compiled into a *pickled* database file explained in the 
following section.

## 1. Pickled Database File and ASCII `*.mat` file

`Pickle` is an out-of-the box Python module used for serializing and de-serializing 
Python object. In this study, it is used to consolidate results from multiple
realization for convenient storage. Specifically, a pickled database file contains 
time-dependent TRACE output for all the realizations. It is a `list` object with 
each element corresponds to realization following the trajectory. Each 
realization is a `numpy.ndarray` containing 8 columns with the rows correspond 
to time step. The columns and their unit are as follow:

 1. time [s]
 2. rftn-20A69R29 [K]: bottom-assembly level temperature
 3. rftn-20A89R29 [K]: mid-assembly level temperature
 4. rftn-20A109R29 [K]: top-assembly level temperature
 5. pn-30A04 [Pa]: pressure reference tap at the upper plenum
 6. pn-1A17R01T01 [Pa]: top pressure probe
 7. pn-1A13R01T01 [Pa]: middle pressure probe
 8. pn-1A01R01T01 [Pa]: bottom pressure probe

The pressure drop can be calculated as follows:

 1. *lower* dP = bottom - middle
 2. *middle* dP = middle - top
 3. *upper* dP = top - reference
 4. *total* dP = bottom - reference

Some time, it is also useful to simply extract a single TRACE variable and 
consolidate all the realizations into a single ASCII file. In this study, 
it was done through post-processing script that produce `*.mat` file.
The file is a space-separated file (created using `np.savetxt` function) with
time as the first column, the base-case output as the second column, followed 
by the rest of the realizations.

## 2. Morris Campaign of 26-parameter TRACE model of FEBA Test No. 216 

The morris screening method was done for the 26-parameter TRACE model of FEBA 
Test No. 216 TRACE model using 40 trajectories and 8 levels. The total runs is 
1'080 runs. The post-processing scripts for this campaign include:

 1. `xtv2csv_morris.py`: script to convert all xtv files into csv files for the
    requested variables
 2. `csv2pdb_morris.py`: script to convert all csv files into a single pickled 
    database file only for TRACE graphic variable `rftn-20A89R29` (i.e., 
    mid-assembly heat structure temperature)
 3. `common_data_morris.py`: module which contains shared global variables

The first two scripts can be executed by simply passing it to the python 
executable. All the results are produced in `216-morris/` folder. Furthermore, 
all runs corresponding to the design matrix have to be successfully carried out 
already.

The output used in the subsequent analysis is 
`postpro-feba-216-26-morris-40.db` that contains all the Morris trajectories 
output following the previous specification in Section 1.

## 3. Sobol' Campaign of 26-parameter TRACE model of FEBA Test No. 216

Estimating the Sobol' indices for the 26-parameter TRACE model of FEBA Test 
No. 216 was done using sample size of 1'000 (total runs of 28'000 runs). 
The post-processing scripts for this campaign include:

 1. `xtv2csv_sobol_full.py`: Python script to convert xtv/dmx files into csv files
 2. `csv2pdb_sobol_full.py`: Python script to convert csv files into pickled 
    database file

To run `xtv2csv_sobol_full.py` several command line arguments need to be specified 
as follows:

    python xtv2csv_sobol_full.py -c 216 -d ../simulation/feba-sobol_26_1000_sobol.txt -ns <sample start> <sample end>

Note that the number of samples is between 1 to 1000 and can be sliced. This 
was done so the routine can be called in multiple machines to accelerate the 
post-processing. Admittedly, this was a quick and dirty solution to this 
problem. Furthermore, all runs corresponding to the design matrix have to be
successfully carried out already.

`csv2pdb_sobol_full.py` can only be executed if all the available `.csv` files 
from the previous step were successfully created. The script can be executed as 
follow without any command line argument,

    python csv2pdb_sobol_full.py <start_sample> <end_sample>

Successfull execution of the script will create a pickled database file 
`postpro-feba-216-10-sobol-sobol-2000-{<start_sample}-{end_sample}.db` as 
described in **Section 1** for all realizations following the Sobol' design.

Note that in the subsequent post-processing, especially when ranking the 
parameter importance based on Sobol' indices was done, the pickled database 
file was divided into 10 groups. That is, the call to `csv2pdb_sobol_full.py` 
script was done 10 times for multiple batches each with sample size of 100.

All the results are produced in `216-sobol-full/` folder.

## 4. Sobol' Campaign of 10-parameter TRACE model of FEBA Test No. 216

Estimating the Sobol' indices for the 10-parameter TRACE model (screened model) 
of FEBA Test No. 216 was done using sample size of 2'000 (total runs of 24'000 
runs). The post-processing scripts for this campaign include:

 1. `xtv2csv_sobol.py`: Python script to convert xtv (dmx) files into csv files
 2. `csv2pdb_sobol.py`: 
 3. `pdb2mat_sobol.py`: Python driver script to convert the pickled database 
    file into mat file containing all realization for a single TRACE graphic 
    variable in uniform time grid.
 4. `common_data_sobol.py`: module which contains shared global variables

To run `xtv2csv_sobol.py` several command line arguments need to be specified 
as follows:

    python xtv2csv_sobol.py -c 216 -d ../simulation/feba-sobol_26_10_sobol.txt -ns <sample start> <sample end>

Note that the number of samples is between 1 to 2000 and can be sliced. This 
was done so the routine can be called in multiple machines to accelerate the 
post-processing. Admittedly, this was a quick and dirty solution to this 
problem. Furthermore, all runs corresponding to the design matrix have to be
successfully carried out already.

`csv2pdb_sobol.py` can only be executed if all the available `.csv` files from 
the previous step were successfully created. The script can be executed as 
follow without any command line argument,

    python csv2pdb_sobol.py

Successfull execution of the script will create a pickled database file 
`postpro-feba-216-10-sobol-sobol-2000.db` as described in **Section 1** for all 
ealizations following the Sobol' design.

The script `pdb2mat_sobol.py` is used to collect a single TRACE graphic 
variable from each of the realizations, put them into a uniform time grid, and 
save the file into an ASCII file (`*.mat`). It can be called with the following
command,

    python pdb2mat_sobol.py <pickled database file> <trace_variable>

Where `<trace_variable>` is a valid available TRACE variable: `rftn-20A69R29`,
`rftn-20A89R29`, `rftn-20A109R29`, `pn-30A04`, `pn-1A17R01T01`, `pn-1A13R01T01`,
and `pn-1A01R01T01`.

Successfull execution of the script will create an ASCII space-separated file 
`postpro-feba-216-10-sobol-sobol-2000-unif_<trace_variable>.mat` as described 
in **Section 1** for all realizations following the Sobol' design.

Note that in the subsequent post-processing relevant to the results presented
in the paper, only `rftn-20A89R29` (mid-assembly level temperature) was used.

All the results are produced in `216-sobol-screened/` folder.

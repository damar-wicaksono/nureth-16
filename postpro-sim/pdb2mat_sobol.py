# -*- coding: utf-8 -*-
# title     : csv2pdb_sobol.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2015
# purpose   : Python driver script to convert the pickled database file 
#           : into mat file containing all realization for a single TRACE 
#           : graphic variable in uniform time grid. First column is time step,
#           : second column is the base case run, and the rest are the 
#           : realization 
# usage     : > python pdb2mat_sobol.py <pickled database> <trace_variable>
# py_version: 3.3.4 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
#

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
import pickle

import common_data_sobol as common_data
from sa_module import sobol
from sa_module import trace

def main():
    
    # Common variables
    max_t = common_data.max_t 
    delta_t = common_data.delta_t

    # Decide the requested trace variable
    key = sys.argv[-1]
    if key == "rftn-20A69R29":
        col = 1
        key = "unif_bot"
    elif key == "rftn-20A89R29":
        col = 2
        key = "unif_mid"
    elif key == "rftn-20A109R29":
        col = 3
        key = "unif_top"
    elif key == "pn-30A04":
        col = 4
        key = "unif_pref"
    elif key == "pn-1A17R01T01":
        col = 5
        key = "unif_ptop"
    elif key == "pn-1A13R01T01":
        col = 6
        key = "unif_pmid"
    elif key == "pn-1A01R01T01":
        col = 7
        key = "unif_pbot"
    else:
        raise ValueError("TRACE variable not supported!")
    
    # Open and add the base case
    unif_data = np.arange(0.0, max_t, delta_t)  # the uniform time
    base_fullname = "./216-base/febaTrans216_base.csv"
    base_run = np.loadtxt(base_fullname, delimiter=",", skiprows=2) 
    unif_data = np.column_stack((unif_data, trace.unif_time.create(base_run,
                                                                   col, 
                                                                   max_t, 
                                                                   delta_t)))
    # Open the pickled database
    inpfullname = sys.argv[-2]
    with open(inpfullname, "rb") as file_object:
         runs = pickle.load(file_object)

    # Loop over the list of realizations
    for run in runs:
         unif_data = np.column_stack((unif_data, 
                                      trace.unif_time.create(run, 
                                                             col, 
                                                             max_t, delta_t)))

    # Save to a file 
    otppathname = inpfullname.split("/")[:-1]
    otpfilename = inpfullname.split("/")[-1].split(".")[0]
    otpfilename = "{}-{}.mat" .format(otpfilename, key)
    otpfullname = "{}/{}" .format("/".join(otppathname), otpfilename)
    np.savetxt(otpfullname, unif_data, fmt="%1.4e")

if __name__ == "__main__":
    main()

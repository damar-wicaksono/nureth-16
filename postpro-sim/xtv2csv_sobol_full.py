# -*- coding: utf-8 -*-
# title     : xtv2csv_sobol_full.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2014
# purpose   : Python script to convert xtv (or dmx) file into csv files
#           : for Sobol' campaign of 26-parameter TRACE FEBA model
# usage     : > python xtv2csv_sobol.py -c 216 \
#                                       -d ../simulation/feba-sobol_26_1000_sobol.txt \
#                                       -ns <sample start> <sample end>
# py_version: 3.3.4 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
################################################################################

import os
import sys
sys.path.append(os.path.abspath("../"))
import numpy as np
import subprocess

import common_data_sobol as common_data
from sa_module import runutils
from sa_module import trace

def main():

    #####################################
    # Get command line input parameters #
    #####################################
    case_no, design_filename, num_sample, num_procs, second_order = \
        runutils.get_input.sobol()

    #############################
    # read listVars input files #
    #############################
    vars_inp_filename = "{}/febaVars{}.inp" .format(
        common_data.base_dir, case_no
    )
    vars_dict = trace.list_to_dict.inpvars_to_dict(vars_inp_filename)
    num_params = len(vars_dict)

    ######################
    # Run TRACE in batch #
    ######################
    run_count = (num_sample[0] - 1) * (2 * num_params + 2)
    if second_order:
        total_runs = (num_sample[1] - num_sample[0] + 1) * num_params
        run_count = run_count + (num_params + 2)
        block_count = num_params
    else:
        total_runs = (num_sample[1] - num_sample[0] + 1) * (num_params + 2)
        block_count = (num_params + 2)
    sample = [_ for _ in range(num_sample[0], num_sample[1] + 1)]
    j = 0
    i = run_count + 1

    for n in range(1, total_runs+1):

        if block_count <= 0:
            if second_order:
                block_count = num_params
                j += 1
                i = (sample[j] - 1) * (2 * num_params + 2) + (num_params + 2) + 1
            else:
                block_count = num_params + 1
                j += 1
                i = (sample[j] - 1) * (2 * num_params + 2) + 1
        else:
            block_count -= 1 

        run_key = "run_sobol_{}_{}" .format(common_data.scheme, i)
        run_dir = "{}/{}" .format(common_data.case_dir_full, run_key)
        run_scratch_dir = "{}/{}" .format(common_data.scratch_case_dir, run_key)
        run_filename = "febaTrans{}_{}" .format(case_no, run_key)

        # Convert to csv file
        select_vars = ["rftn-20A69R29", "rftn-20A89R29",
                       "rftn-20A109R29", "pn-30A04",
                       "pn-1A17R01T01", "pn-1A13R01T01",
                       "pn-1A01R01T01"]

        trace.trace_postpro.xtv_to_csv(
            common_data.aptplot_exec,
            run_filename,
            select_vars,
            case_no,
            i,
            xtv_ext="dmx",
            workdir=run_dir
        )

        csv_filename = "{}.csv" .format(run_filename)
        csv_fullname1 = "{}/{}" .format(run_dir, csv_filename)
        csv_fullname2 = "{}/{}" .format(common_data.postpro_case_dir_full, csv_filename)
        subprocess.call(["mv", csv_fullname1, csv_fullname2])

        i += 1

if __name__ == "__main__":
    main()

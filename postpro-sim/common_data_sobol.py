# -*- coding: utf-8 -*-
# Common Data
max_t = 600.0
delta_t = 0.1

cases = ["214", "216", "218", "220", "222", "223"]
case_no = "216"
scheme = "sobol"
######################
# Common Executables #
######################
trace_exec = "trace_v5.0p3.uq_extended"
xtv2dmx_exec = "xtv2dmx_v6.5.2_inst01.sh"
aptplot_exec = "aptplot_v6.5.2_inst01.sh"

######################
# Common Directories #
######################
setf = "simulation"
workspace_dir = "/afs/psi.ch/project/stars/workspace/RND/"\
    "SB-RND-ACT-006-13/WD41/projects"
#scratch_dir = "/afs/psi.ch/group/lrs/scratch/grp.lrs.scr001.nb/wicaksono_d"
#scratch_dir = "/home/scratch/wicaksono_d"
scratch_dir = "/afs/psi.ch/project/stars/workspace/RND/"\
    "SB-RND-ACT-006-13/WD41/codesProject"
project_name = "nureth-16"

workspace_dir_project = "{}/{}" .format(workspace_dir, project_name)
setf_dir = "{}/{}" .format(workspace_dir_project, setf)
base_dir = "{}/base" .format(setf_dir)
case_dir = "{}/{}-sobol-screened" .format(setf_dir, case_no)
case_dir_full = "{}/{}-sobol" .format(setf_dir, case_no)

scratch_dir_project = "{}/{}" .format(scratch_dir, project_name)
scratch_setf_dir = "{}/{}" .format(scratch_dir_project, setf)
scratch_case_dir = "{}/{}-sobol-screened" .format(scratch_setf_dir, case_no)
scratch_case_dir_full = "{}/{}-sobol" .format(scratch_setf_dir, case_no)

postpro_dir = "postpro-sim" 
postpro_case_dir = "{}/{}/{}/{}-sobol-screened" .format(workspace_dir, project_name, postpro_dir, case_no)
postpro_case_dir_full = "{}/{}/{}/{}-sobol-full" .format(workspace_dir, project_name, postpro_dir, case_no)

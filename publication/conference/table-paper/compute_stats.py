#!/usr/bin/env python
# title         : compute_stats.py
# description   : Compute the statistics of model output for full 
#               : (26-parameter) model and screened (10-parameter) model
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python rank_morris.py <pickled_full_outp> <screened_outp> <outp_file>
# py_version    : 3.5
#

import os
import sys
import pickle
import numpy as np
sys.path.append(os.path.abspath("../.."))


__author__ = "Damar Wicaksono"


def stat_summary(y):
    """Compute statistics from an array
    
    :param y: an array of scalar model output
    :return: the mean, median, 1st quartile, 3rd quartile, interquartile range,
        standard deviation, and coefficient of variation
    """
    mean = np.mean(y)
    median = np.percentile(y, 50.)
    quartile1 = np.percentile(y, 25.)
    quartile3 = np.percentile(y, 75.)
    iqr = quartile3 - quartile1
    sd = np.std(y, ddof=1)
    cv = sd/mean
    return [mean, median, quartile1, quartile3, iqr, sd, cv]


def main():
    
    N = 1000            # Number of samples to compute the statistics
    time_ind = 0        # Time index in the output array
    time_max = 499.9    # Maximum time to be considered
    temp_ind = 2        # Relevant temperature index, mid-assembly
    stat_names = ["mean", "median", "1stquartile", 
                  "2ndquartile", "iqr", "sd", "cv"] # statistics names

    # Load Data of Full Model Output
    temp_max_full = np.empty([0])
    temp_ave_full = np.empty([0])
    for i in range(3, 13):
        with open(sys.argv[-1*i], "rb") as sobol_file:
            sobol_full = pickle.load(sobol_file)
        for j in range(len(sobol_full)):
            temp_max_full = np.append(temp_max_full, 
                                      np.max(sobol_full[j][:, temp_ind]))
            temp_ave_full = np.append(temp_ave_full, 
                                      np.trapz(sobol_full[j][:, temp_ind][sobol_full[j][:, time_ind] <= time_max], 
                                               sobol_full[j][:, time_ind][sobol_full[j][:, time_ind] <= time_max]) / (time_max+0.1))
    # Compute the Summary Statistics
    temp_max_full_stats = stat_summary(temp_max_full)
    temp_ave_full_stats = stat_summary(temp_ave_full)

    # Load Data of Screened Model Output
    temp_mid_screened = np.loadtxt(sys.argv[-2])
    temp_max_screened = np.empty([0])
    temp_ave_screened = np.empty([0])
    k = 10  # Number of screened parameters
    for i in range(2, N*(k+2)+1):
        # shift by 2 because the first 2 are the time and base case, resp.
        temp_max_screened = np.append(temp_max_screened,
                                      np.max(temp_mid_screened[:, i]))
        temp_ave_screened = np.append(temp_ave_screened,
                                      np.trapz(temp_mid_screened[:, i][temp_mid_screened[:, time_ind]<=time_max], 
                                               temp_mid_screened[:, time_ind][temp_mid_screened[:, time_ind]<=time_max]) / (time_max+0.1))

    # Compute the Statistics
    temp_max_screened_stats = stat_summary(temp_max_screened)
    temp_ave_screened_stats = stat_summary(temp_ave_screened)

    # Write into a file
    with open(sys.argv[-1], "w") as stat_summary_file:
        stat_summary_file.write("value,stats,params,qoi\n")
        for i, value in enumerate(temp_max_full_stats):
            stat_summary_file.write("{},{},{},{}\n" \
                .format(round(value, 3), stat_names[i], "full", "temp_max"))
        for i, value in enumerate(temp_max_screened_stats):
            stat_summary_file.write("{},{},{},{}\n" \
                .format(round(value, 3), stat_names[i], "screened", "temp_max"))
        for i, value in enumerate(temp_ave_full_stats):
            stat_summary_file.write("{},{},{},{}\n" \
                .format(round(value, 3), stat_names[i], "full", "temp_ave"))
        for i, value in enumerate(temp_ave_screened_stats):
            stat_summary_file.write("{},{},{},{}\n" \
                .format(round(value, 3), stat_names[i], "screened", "temp_ave"))


if __name__ == "__main__":
    main()

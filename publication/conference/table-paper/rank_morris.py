#!/usr/bin/env python
# title         : rank_morris.py
# description   : Create a parameter importance ranking based on Morris mu_star
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python rank_morris.py <pickled_outp> <morris_dm> <outp_file>
# py_version    : 3.5
#

import os
import sys
import pickle
import numpy as np
import pandas as pd
sys.path.append(os.path.abspath("../.."))
from sa_module.morris import analyze
import common   # Contains all the 26 parameter names


__author__ = "Damar Wicaksono"


def main():

    # Read the pickled file of Morris trajectories (26 params, 40 trajectories)
    with open(sys.argv[-3], "rb") as morris_file:
        morris_40 = pickle.load(morris_file)

    # Put the Quantity of Interest (average temperature) in a file
    qoi_temp_ave = np.empty([0])
    time_max = 499.0 # Maximum time to be considered
    time_ind = 0     # Index of the time in the postprocessed output array
    temp_ind = 2     # Index of the time-dependent temperature, the same array
    for i in range(len(morris_40)):
        qoi_temp_ave = np.append(qoi_temp_ave, 
             np.trapz(morris_40[i][:, temp_ind][morris_40[i][:, time_ind] <= time_max],
                      morris_40[i][:, time_ind][morris_40[i][:, time_ind] <= time_max]) / time_max)

    # Compute Elementary effect
    morris_design = np.loadtxt(sys.argv[-2], delimiter=" ")
    ee_data = analyze.morris(morris_design, qoi_temp_ave, 
                             [value for key, value in common.param_names_26.items()], 
                             column=1)
    # Create ranking based on mu_star
    rank = np.empty([len(ee_data["mu_star"])], int)
    arg_rank = np.argsort(ee_data["mu_star"]) # rank based on the mu_star
    rank[arg_rank] = (np.arange(len(ee_data["mu_star"]))+1)[::-1]

    # Write the results into a file
    with open(sys.argv[-1], "w") as morris_rank_file:
        for i, ee_datum in enumerate(ee_data.iterrows()):
            # no., param_names, rank, mu_star, sigma
            morris_rank_file.write("{},{},{:d},{:4.4f},{:4.4f}\n" \
                .format(ee_datum[0],
                        ee_datum[1]["name"],
                        rank[i],
                        ee_datum[1]["mu_star"],
                        ee_datum[1]["sigma"]))


if __name__ == "__main__":
    main()

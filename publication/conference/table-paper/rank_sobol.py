#!/usr/bin/env python
# title         : rank_sobol.py
# description   : Create a parameter importance ranking based on Sobol indices
# author        : WD41, LRS/EPFL/PSI
# date          : 2015
# usage         : python rank_sobol.py <pickled_outputs>
# py_version    : 3.5
#

import os
import sys
import pickle
import numpy as np
sys.path.append(os.path.abspath("../.."))
from sa_module import sobol
import common           # Contains all the 26 parameter names


__author__ = "Damar Wicaksono"


def main():
    # Load all the required data and compute the average temperature
    qoi_temp_ave = np.empty([0])
    time_max = 499.0 # Maximum time to be considered
    time_ind = 0     # Index of the time in the postprocessed output array
    temp_ind = 2     # Index of the time-dependent temperature, the same array
    
    for i in range(2, 12):
        with open(sys.argv[-1*i], "rb") as sobol_file:
            sobol_full = pickle.load(sobol_file)
        for j in range(len(sobol_full)):
            qoi_temp_ave = np.append(qoi_temp_ave, 
                                     np.trapz(sobol_full[j][:, temp_ind][sobol_full[j][:, time_ind] <= time_max], 
                                              sobol_full[j][:, time_ind][sobol_full[j][:, time_ind] <= time_max]) / time_max)

    # Compute the Sobol' indices using the Saltelli et al. estimator
    k = 26              # Number of Parameters
    N = 1000            # Number of Samples
    N_bootstrap = 10000 # Number of bootstrap replications
    qoi_temp_ave_dict = sobol.sobol_saltelli.read(qoi_temp_ave, N, k)
    qoi_temp_ave_ind, qoi_temp_ave_bootstrap = sobol.indices.evaluate_1st(
        qoi_temp_ave_dict, True, 20151418, N_bootstrap)

    # Rank the parameter importance
    # Main-effect indices, possibly less than zero due to MC error
    main_indices = []
    for i in range(k):
        key = "S{}" .format(i+1)
        main_indices.append(round(qoi_temp_ave_ind[key], 2))
    main_indices = np.array(main_indices)
    rank = np.empty([k], int)
    rank[np.argsort(main_indices)] = (np.arange(k) + 1)[::-1]
    # Upper bound of the percentile confidence interval
    main_indices_upper = []
    for i in range(k):
        key = "S{}_CI_%" .format(i+1)
        main_indices_upper.append(round(qoi_temp_ave_ind[key][1], 3))
    main_indices_upper = np.array(main_indices_upper)
    rank_upper = np.empty([k], int)
    rank_upper[np.argsort(main_indices_upper)] = (np.arange(k) + 1)[::-1]
    # Make the rank, if negative used the upper bound of Percentile CI
    for i in range(k):
        #if main_indices > 0.0:
        if rank[i] != rank_upper[i]:
            rank[i] = rank_upper[i]

    # Write the results into a file
    with open(sys.argv[-1], "w") as sobol_rank_file:
        sobol_rank_file.write("no,param_names,rank,si,si_ci_%_lower,si_ci_%_upper,sti,sti_ci_%_lower,sti_ci_%_upper\n")
        for i in range(1, k+1):
            key1 = "S{}" .format(i)
            key2 = "S{}_CI_%" .format(i)
            key3 = "ST{}" .format(i)
            key4 = "ST{}_CI_%" .format(i)
            # no., param_names, main-effect, %CI, total-effect, %CI
            sobol_rank_file.write("{},{},{},{:4.2f},{:4.2f},{:4.2f},{:4.2f},{:4.2f},{:4.2f}\n" \
                .format(i, 
                        common.param_names_26[i],
                        rank[i-1],
                        qoi_temp_ave_ind[key1],
                        round(qoi_temp_ave_ind[key2][0], 2),
                        round(qoi_temp_ave_ind[key2][1], 2),
                        qoi_temp_ave_ind[key3],
                        round(qoi_temp_ave_ind[key4][0], 2),
                        round(qoi_temp_ave_ind[key4][1], 2)))


if __name__ == "__main__":
    main()

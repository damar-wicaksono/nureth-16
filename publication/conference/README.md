# Makefile to Create Plots and Data for Table

## Dependency

 1. `R version 3.3.1` (2016-06-21)
 2. R package `packrat version 0.4.7-1`
 3. Anaconda Python v3.5

Make sure that the package `packrat` is installed as it will resolve all the 
dependencies on other required packages to produce the plots. The base 
installation of the Anaconda Python is sufficient.

to install an R package from source, use the following command from the 
R terminal

    install.packages("<path to source>", repos = NULL)

## Usage

The scripts to create the plots and generate results used in the tables in 
the paper is consolidated using `Makefile`.

To generate Fig. 2 to Fig. 13, use the following command:

    make fig-x

where `x` is the number `2` to `13`

To generate data used in creating the tables use the following command:

    make table-x

where `x` is the number `3` or `4`.

To generate all the figures and the tables, use:

    make all

To reset all the results, use:

    make clean

## Notes

 - `Fig-1` is created using Microsot Powerpoint 2013
 - `Fig-2` to `Fig-13` is created using `R`
 - `R` scripts for `Fig-4`, `Fig-5`, `Fig-8` and `Fig-9` will produce `pdf` 
    files which were then directly edited using Adobe Illustrator to modify 
    the legend in convenient manner
 - data for `table-2` and `table-3` is obtained by python scripts 

#
# title     : fig-7.R
# purpose   : Driver script to create Fig. 7
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Fig. 7 ----------------------------------------------------------------------
# A boxplot of Sobol' indices for each parameter with the time of quenching
# as the quantify of interest using Janon estimator

# load the required postprocessed data: 
#   1. bootstrap data of 10'000 repl., Janon est., and a sample size of 2'000
#   2. the estimated indices using the Janon est. with a sample size of 2'000
# 
# load the required data:
inpfullnames <- c("../../../postpro-py/data/tquench-bootstrap-2000-janon.csv",
                  "../../../postpro-py/data/tquench-indices-2000-janon.csv")
# make the plot for PCT as the QoI for the sensitivity indices
qoi <- "tquench"    
# create the target filename for the plot
otpfullname <- "../figures/fig-7/7-paper-sobol_tquench.png"
# source the code to make the plot
source("./Rscripts/paper-sobol_tquench.R")
# clean up all the variables created
rm(list = ls())

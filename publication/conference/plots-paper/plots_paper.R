#
# title     : plots_paper.R
# purpose   : The main driver script with command line argument to create plots
#           : appeared in the article
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# `p` is the canonical variable name for the plot
# Set the working directory
setwd(paste(getwd(), "/plots-paper", sep = ""))

# Initialize (or Activate) the Packrat package --------------------------------
#packrat::init(getwd())
packrat::on()

# Install Required Packages, do it only once with packrat ---------------------
# `packrat::snapshot()` seems to fail if the fda package is installed first
#install.packages("ggplot2")
#packrat::snapshot()
#install.packages("fda")
#packrat::snapshot()
#install.packages("extrafont")
#packrat::snapshot()
#install.packages("plyr")
#packrat::snapshot()
#install.packages("optparse")
#packrat::snapshot()

# Load Required Packages ------------------------------------------------------
library("fda")
library("ggplot2")
library("extrafont")
library("plyr")
library("optparse")
font_import()   # import the fonts installed on the system
loadfonts()     # only necessary in session where you ran font_import()

# Parse command line argument -------------------------------------------------
option_list <- list(make_option(c("-f", "--figure"), type = "integer",
                                default = NULL,
                                help = "Figure to create (2-13)",
                                metavar = "integer"),
                    make_option(c("-a", "--all"), action = "store_true",
                                default = FALSE,
                                help = "Create all figures (2-13)"));

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)

# Check if the required argument is given
if (is.null(opt$figure) & !opt$all) {
    print_help(opt_parser)
    stop("The argument must be supplied", call. = FALSE)
} else if (!is.null(opt$figure)) {
    if(!is.element(opt$figure, seq(2,13))) {
        print_help(opt_parser)
        stop("Only figure 2 to 13", call. = FALSE)
    }
}

if (opt$all == TRUE){
    for (i in seq(2,13)) {
        driver <- paste("./", "fig-", i, ".R", sep = "")
        source(driver)
    }
} else {
    driver <- paste("./", "fig-", opt$figure, ".R", sep = "")
    source(driver)
}
#
# title     : paper-tc_fpc.R
# purpose   : Script to create Figs. 10 and 12 of the NURETH-16 paper
#           : The fPCs and their effect on the mean function perturbation
#           : (+/- 2 * standard deviation of the component scores)
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Load the required user-made functions ---------------------------------------
source("./Rscripts/plot_harmonic.R")
source("./Rscripts/plot_perturbation.R")
source("./Rscripts/multiplot.R")

# Read the 1st fPC ------------------------------------------------------------
registered_curves_pca <- readRDS(inpfullname)

# Make the plot ---------------------------------------------------------------
p1 <- plot_harmonic(registered_curves_pca, harmonic)
p2 <- plot_perturbation(registered_curves_pca, harmonic)

# Save the plot to the target filename ----------------------------------------
png(otpfullname, width=14.00, height=8.27, units = "in", res=600)
print(multiplot(p1, p2, cols=2))
dev.off()

#
# title     : fig-3.R
# purpose   : Driver script to create Fig. 3
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Fig. 3 ----------------------------------------------------------------------
# Evolution of main-effect indices for PCT with 2 estimators
#
# Required postprocessed data:
#   1. the running indices (estimated Sobol' indices as a function of samples)
#      estimated using the Saltelli et al. estimator
#   2. the running indices estimated using the Janon et al. estimator
#
# load the required data
inpfullnames <- c("../../../postpro-py/data/tempmax-running_indices-saltelli.txt",
                  "../../../postpro-py/data/tempmax-running_indices-janon.txt")
estimator_names <- c("Saltelli et al.", "Janon et al.")
# create the target filename for the plot
otpfullname <- "../figures/fig-3/3-paper-tempmax_running_indices_main.png"
# Load user made function to create a tidy dataframe
source("./Rscripts/tidy_runindex.R")
# source the code to make the plot
source("./Rscripts/paper-tempmax_running_indices_main.R")
# clean up all the variables created
rm(list = ls())
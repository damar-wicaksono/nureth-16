#' Tidy dataframe of functional principal components
#' 
#' Passed the fpc dataframe and the max number of harmonics to be plotted
#' The function is tailored for facet plotting purpose
#' 
#' @param pca_df: dataframe. The dataframe with the fpc results
#' @param harmonic: int. the number of harmonic to be compiled
#' @return p: a ggplot2. the Boxplot of sensitivity indices
#'
#' 
tidy_fpc_df <- function(pca_df, harmonic) {
    unif_time <- as.numeric(pca_df$meanfd$fdnames$time)
    
    tidy_fpc_df <- data.frame(time = c(),
                              value = c(),
                              fpc = c(),
                              category = c(),
                              type = c())
    
    for (i in 1:harmonic) {
        harmonics <- unname(eval.fd(unif_time, pca_df$harmonics[i]))
        mean_temp <- unname(eval.fd(unif_time, pca_df$meanfd))
        temp <- unname(eval.fd(unif_time, pca_df$harmonics[i]))
        dev_temp <- 2 * sd(pca_df$scores[, i]) * temp
        lower_temp <- mean_temp - dev_temp
        upper_temp <- mean_temp + dev_temp
        tidy_fpc_df <- rbind(tidy_fpc_df, data.frame(time = unif_time,
                                                     value = harmonics,
                                                     fpc = paste("fpc", 
                                                                 i, 
                                                                 sep = ""),
                                                     category = "Harmonics",
                                                     type = "harmonics"))
        tidy_fpc_df <- rbind(tidy_fpc_df, data.frame(time = unif_time,
                                                     value = mean_temp,
                                                     fpc = paste("fpc", 
                                                                 i, 
                                                                 sep = ""),
                                                     category = "Perturbation",
                                                     type = "Mean"))
        tidy_fpc_df <- rbind(tidy_fpc_df, data.frame(time = unif_time,
                                                     value = lower_temp,
                                                     fpc = paste("fpc", 
                                                                 i, 
                                                                 sep = ""),
                                                     category = "Perturbation",
                                                     type = "Lower"))
        tidy_fpc_df <- rbind(tidy_fpc_df, data.frame(time = unif_time,
                                                     value = upper_temp,
                                                     fpc = paste("fpc", 
                                                                 i, 
                                                                 sep = ""),
                                                     category = "Perturbation", 
                                                     type = "Upper"))
    }
    tidy_fpc_df
}
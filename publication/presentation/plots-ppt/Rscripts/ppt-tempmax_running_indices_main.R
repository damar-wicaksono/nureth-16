#
# title     : ppt-tempmax_running_indices_main.R
# purpose   : Script to create Fig. 7 of the NURETH-16 presentation (slide 13)
#           : Running indices of main-effect indices using 2 different 
#           : estimators with max. temperature as QoI
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Create IDs for indices to be excluded from plotting (i.e., total) -----------
total <- c()
for (i in 1:10) {
    total[i] <- paste("ST", i, sep = "")
}

# Create a tidy data frame ----------------------------------------------------
# Load user-made function to create a tidy dataframe
source("../../conference/plots-paper/Rscripts/tidy_runindex.R")
df_runindex <- tidy_runindex(inpfullnames[1], estimator_names[1],
                             inpfullnames[2], estimator_names[2])

# Plot the running indices ----------------------------------------------------
p <- ggplot(subset(df_runindex, !index %in% total), 
            aes(x = samples, y = value, colour = index)) # exclude total
p <- p + facet_wrap(~estimator) + geom_line()
p <- p + scale_y_continuous(breaks = seq(-0.1, 1.0, 0.10), 
                            limits = c(-0.1, 1.0))

# Set the correct font
p <- p + theme_bw(base_family = "Raleway", base_size = 12)
# Set the color of the facet box
p <- p + theme(strip.background = element_rect(fill = "#ffffb3", 
                                               colour = "black",
                                               size = 0.50))
# Use this color pallete for 10 categorical variables
new_colors <- c("#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3",
                "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd")
p <- p + scale_color_manual(values = new_colors)
# Suppress the legend
p <- p + theme(legend.position = "None")
# Edit the x-axis and y-axis labels
p <- p + labs(y = "Index Value [-]",
              x = "Number of MC Samples")

# Save into a file ------------------------------------------------------------
png(otpfullname, width = 7.04, height = 2.2, units = "in", res = 600)
print(p)
dev.off()

# title     : fig-14.R
# purpose   : Driver script to create Fig. 14 in slide 18
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Fig. 14 (Slide 19) ----------------------------------------------------------
# The 1st principal component of the registered mid-assembly temperature evol.
#
# Required postprocessed data:
#   1. the Rds file containing `fd.pc` object from the PCA of registered mid-
#      assembly temperature functional data
#
# load the required data
inpfullname <- c("../../../postpro-R/mid-2000/sobol-tc-mid-pca-registered.rds")
n_fpc <- "fpc1"
# create the target filename for the plot
otpfullname <- "../figures/14-ppt-fpc1.png"
# source the code to make the plot
source("./Rscripts/ppt-fpc.R")
# do cleanup of variables created
rm(list = ls())

fig-5  := figures/5-ppt-randomcurves.png
fig-8  := figures/8-ppt-tempmax_running_indices_main.png
fig-9  := figures/9-ppt-illustrate_influential_noninfluential.png
fig-10 := figures/10-ppt-randomcurves_grey.png
fig-11 := figures/11-ppt-sobol_pct.png
fig-12 := figures/12-ppt-sobol_tquench.png
fig-13 := figures/13-ppt-fpc_facet.png
fig-14 := figures/14-ppt-fpc1.png
fig-15 := figures/15-ppt-sobol_fpc1.png
fig-16 := figures/16-ppt-fpc2.png
fig-17 := figures/17-ppt-sobol_fpc2.png

all: fig5 fig8 fig9 fig10 fig11 fig12 fig13 fig14 fig15 fig16 fig17

clean:
	rm --force ${fig-5}
	rm --force ${fig-8}
	rm --force ${fig-9}
	rm --force ${fig-10}
	rm --force ${fig-11}
	rm --force ${fig-12}
	rm --force ${fig-13}
	rm --force ${fig-14}
	rm --force ${fig-15}
	rm --force ${fig-16}
	rm --force ${fig-17}

.PHONY: all clean

# Main scripts
calling-script: plots-ppt/plots_ppt.R

# Recipe to create fig-5 (slide 6)
fig5: ${fig-5}
${fig-5}: calling-script \
		  plots-ppt/fig-5.R \
          plots-ppt/Rscripts/ppt-randomcurves.R
	Rscript plots-ppt/plots_ppt.R -f 5

# Recipe to create fig-8 (slide 13)
fig8: ${fig-8}
${fig-8}: calling-script \
		  plots-ppt/fig-8.R \
          plots-ppt/Rscripts/ppt-tempmax_running_indices_main.R
	Rscript plots-ppt/plots_ppt.R -f 8

# Recipe to create fig-9 (slide 14)
fig9: ${fig-9}
${fig-9}: calling-script \
		  plots-ppt/fig-9.R \
          plots-ppt/Rscripts/ppt-illustrate_influential_noninfluential.R
	Rscript plots-ppt/plots_ppt.R -f 9

# Recipe to create fig-10 (slide 17)
fig10: ${fig-10}
${fig-10}: calling-script \
		  plots-ppt/fig-10.R \
          plots-ppt/Rscripts/ppt-randomcurves.R
	Rscript plots-ppt/plots_ppt.R -f 10

# Recipe to create fig-11 (slide 17)
fig11: ${fig-11}
${fig-11}: calling-script \
		  plots-ppt/fig-11.R \
		  plots-ppt/Rscripts/ppt-sobol.R
	Rscript plots-ppt/plots_ppt.R -f 11

# Recipe to create fig-12 (slide 17)
fig12: ${fig-12}
${fig-12}: calling-script \
		  plots-ppt/fig-12.R \
		  plots-ppt/Rscripts/ppt-sobol.R
	Rscript plots-ppt/plots_ppt.R -f 12

# Recipe to create fig-13 (slide 18)
fig13: ${fig-13}
${fig-13}: calling-script \
		  plots-ppt/fig-13.R \
		  plots-ppt/Rscripts/ppt-fpc_facet.R
	Rscript plots-ppt/plots_ppt.R -f 13

# Recipe to create fig-14 (slide 19)
fig14: ${fig-14}
${fig-14}: calling-script \
		  plots-ppt/fig-14.R \
		  plots-ppt/Rscripts/ppt-fpc.R
	Rscript plots-ppt/plots_ppt.R -f 14

# Recipe to create fig-15 (slide 19)
fig15: ${fig-15}
${fig-15}: calling-script \
		  plots-ppt/fig-15.R \
		  plots-ppt/Rscripts/ppt-sobol.R
	Rscript plots-ppt/plots_ppt.R -f 15

# Recipe to create fig-16 (slide 20)
fig16: ${fig-16}
${fig-16}: calling-script \
		  plots-ppt/fig-16.R \
		  plots-ppt/Rscripts/ppt-fpc.R
	Rscript plots-ppt/plots_ppt.R -f 16

# Recipe to create fig-17 (slide 20)
fig17: ${fig-17}
${fig-17}: calling-script \
		  plots-ppt/fig-17.R \
		  plots-ppt/Rscripts/ppt-sobol.R
	Rscript plots-ppt/plots_ppt.R -f 17
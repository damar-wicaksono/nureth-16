# Makefile to Create Figures appeared in Powerpoint

## Dependency

 1. `R version 3.3.1` (2016-06-21)
 2. R package `packrat version 0.4.7-1`
 3. Raleway font from google

Make sure that the package `packrat` is installed as it will resolve all the 
dependencies on other required packages to produce the plots. It is recommended
to run the `make` in Windows after the font Raleway has been installed.

## Usage

The scripts to create the plots and generate results used in the tables in 
the paper is consolidated using `Makefile`.

To generate Fig. 5 and Fig. 7 to Fig. 17, use the following command:

    make fig-x

where `x` is the number `5` or `7` to `17`

To generate all the figures and the tables, use:

    make all

To reset all the results, use:

    make clean

## Notes

 - `Fig-1` is taken from FEBA report
 - `Fig-2` is taken from TRACE theory manual
 - `Fig-3` is taken from uncertainty propagation results of FEBA submitted to
    PREMIUM benchmark (SB-RND-ACT-006.13.002)
 - `Fig-4` is taken from NUTHOS-10 FDA presentation (SB-RND-ACT-006.13.003)
 - `Fig-6` is taken from NUTHOS-10 FDA presentation (SB-RND-ACT-006.13.003)

# -*- coding: utf-8 -*-
# title     : run_morris_feba.py
# author    : WD41, LRS/EPFL/PSI
# date      : April 2014
# purpose   : Python driver script to execute multiple runs of TRACE 
#           : with input parameters based on the factorial Morris design matrix
# usage     : > python 2-run_morris.py
#                   -c  [case]
#                   -nr [no. of trajectories]
#                   -nq [no. of levels]
#                   -ns [2 seed numbers]
#                   -np [no. of processors]
# py_version: 3.4.2 (Anaconda Python Distribution,
#             https://store.continuum.io/cshop/anaconda)
################################################################################

import os
import subprocess
import numpy as np

import common_data_morris as common_data
from sa_module import runutils
from sa_module import trace

def main():

    #####################################
    # Get command line input parameters #
    #####################################
    case_no = common_data.case_no
    num_trajectories = 40
    num_levels = 8
    num_procs = 13
    
    #############################
    # read listVars input files #
    #############################
    vars_inp_filename = "{}/febaVars{}.inp" .format(
        common_data.base_dir, case_no
    )
    vars_dict = trace.list_to_dict.inpvars_to_dict(vars_inp_filename)
    num_params = len(vars_dict)
    trace.create_keys.create_keys(vars_dict, keyword="$")

    #################################################
    # Rescale Sobol-Saltelli Design and write it    #
    #################################################

    filename = "Morris_Unscaled_1000_40_8_26_592006_15091518.txt" 
    fullname = "{}/{}" .format(common_data.setf_dir, filename)

    # Create and Write Morris Scaled Design Matrix
    morris_sample_rescaled = trace.list_to_dict.rescale_design(
        vars_dict, fullname
    )
    sample_filename = "feba-morris-scaled_{}_{}_{}.txt" .format(
        case_no,
        num_params, 
        num_trajectories
    )
    sample_fullname = "{}/{}" .format(common_data.case_dir, sample_filename)
    
    np.savetxt(sample_fullname, morris_sample_rescaled, fmt="%1.6e")

    D_matrix_rescaled = np.loadtxt(sample_fullname)

    ######################
    # Load template file #
    ######################
    tracin_filename = "febaTrans{}.inp" .format(case_no)
    tracin_fullname = "{}/{}" .format(common_data.base_dir, tracin_filename)
    template_filename = "febaTrans{}.template" .format(case_no)
    template_fullname = "{}/{}" .format(common_data.case_dir, template_filename)

    trace.write_template.write_template(
        vars_dict, 
        tracin_fullname, 
        template_fullname
    )

    with open(template_fullname, "rt") as template_file:
        template = template_file.read()

    ######################
    # Run TRACE in batch #
    ######################
    #total_runs = num_trajectories * (num_params + 1)
    total_runs = 30
    run_count = 1051

    for batch_iter in trace.batch_exec.batch_run(total_runs, num_procs):

        # Reset the following list for each batch runs
        log_files = []       # Bunch of logfiles
        run_dirs = []        # Bunch of working run directories
        scratch_dirs = []    # Bunch of scratch run directories
        trc_cmds = []        # Bunch of TRACE commands
        xtv_cmds = []        # Bunch of xtv conversion command
        sample_nums = []     # Bunch of sample nums

        scratch_xtvs = []
        link_xtvs = []
        dif_files = []
        tpr_files = []
        ech_files = []

        ###########################################################
        # Create TRACIN and Run for Each Parameter Set in a Batch #
        ###########################################################
        for n in batch_iter:
            i = run_count + n
            list_keys = trace.create_keys.assign_keys(
                vars_dict,
                D_matrix_rescaled[i-1,:]
            )

            run_key = "run_40_8_{}" \
                .format(i)
            run_dir = "{}/{}" .format(common_data.case_dir, run_key)
            run_scratch_dir = "{}/{}" \
                .format(common_data.scratch_case_dir, run_key)
            run_filename = "febaTrans{}_{}" .format(case_no, run_key)
            run_dif_fullname = "{}/{}.dif" .format(run_dir, run_filename)
            run_tpr_fullname = "{}/{}.tpr" .format(run_dir, run_filename)
            run_ech_fullname = "{}/{}.echo" .format(run_dir, run_filename)
            run_inp_fullname = "{}/{}.inp" .format(run_dir, run_filename)
            link_xtv_fullname = "{}/{}.xtv" .format(run_dir, run_filename)
            scratch_xtv_fullname = "{}/{}.xtv" \
                .format(run_scratch_dir, run_filename)
            scratch_dmx_fullname = "{}/{}.dmx" \
                .format(run_scratch_dir, run_filename)
            scratch_xtvs.append(scratch_xtv_fullname)
            link_xtvs.append(link_xtv_fullname)
            dif_files.append(run_dif_fullname)
            tpr_files.append(run_tpr_fullname)
            ech_files.append(run_ech_fullname)

            if not os.path.exists(run_dir):
                os.makedirs(run_dir)
            if not os.path.exists(run_scratch_dir):
                os.makedirs(run_scratch_dir)

            print('###########')
            print('Creating TRACE inp. file for Morris Test No. {} on sample {}.'\
                  .format(case_no, i))

            with open(run_inp_fullname, 'wt') as tracin:
                print(template % list_keys, file=tracin)

            run_dirs.append(run_dir)
            scratch_dirs.append(run_scratch_dir)
            sample_nums.append(str(i))

            # List of logfiles
            log_file = open('{}/run_{}.log' .format(run_dir, str(i)), 'wt')
            log_files.append(log_file)
            # TRACE commands
            trc_cmd = [common_data.trace_exec, '-p', run_filename]
            trc_cmds.append(trc_cmd)
            # xtv conversion command to save even more space
            xtv_filename = '{}.xtv' .format(run_filename)
            dmx_filename = '{}.dmx' .format(run_filename)
            xtv_cmd = [common_data.xtv2dmx_exec, '-r', xtv_filename, '-d', dmx_filename]
            xtv_cmds.append(xtv_cmd)
            # XTV softlink to save space in the project workspace
            subprocess.call(['ln', '-s', scratch_dmx_fullname, run_dir])
            subprocess.call(['ln', '-s', scratch_xtv_fullname, run_dir])

        #############################################
        # Executing TRACE for each batch of samples #
        #############################################
        trace.batch_exec.exec_trace(
            log_files, 
            run_dirs, 
            scratch_dirs, 
            trc_cmds, 
            xtv_cmds, 
            num_procs, 
            sample_nums, 
            case_no
        )

        ######################################
        # Do Some XTV cleanup in the scratch #
        ######################################
        for scratch_xtv, link_xtv, tpr_file, dif_file, ech_file in zip(
            scratch_xtvs,
            link_xtvs,
            tpr_files,
            dif_files,
            ech_files):
            subprocess.call(['rm', '-f', scratch_xtv])
            subprocess.call(['rm', '-f', link_xtv])
            subprocess.call(['rm', '-f', tpr_file])
            subprocess.call(['rm', '-f', dif_file])
            subprocess.call(['rm', '-f', ech_file])

if __name__ == "__main__":
    main()

"""sa_module.samples: Modules containing functions to generate sample using
various sampling schemes. Additional functions to rescale the sample according 
to other distributions other than uniform with [0, 1] range.
"""

from . import design_srs
from . import design_lhs
from . import design_sobol
from . import rescale

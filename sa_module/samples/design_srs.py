"""Module to generate Simple Random Sample
"""
import numpy as np

def create(n, d, seed):
    r"""Generate :math:`(d \times d)` samples using Simple Random sampling

    The function returns a numpy array of `n`-rows and `d`-dimension
    filled with randomly generated number from uniform distribution 
    of [0, 1].

    :param n: (int) The number of samples
    :param d: (int) The number of dimension
    :param seed: (int) The random seed number
    :returns: (ndarray) A numpy array of `n`-by-`d` filled with uniformly 
        generated random numbers
    """
    np.random.seed(seed)

    return np.random.rand(n, d)

"""Module to generate Sobol Sequence for Quasi-Monte Carlo Simulation
"""
import subprocess
import numpy as np


def create(n, d, generator, dirnumfile):
    """Generate `d`-dimensional Sobol sequence of length `n`

    This function serves only as a wrapper to call a generator on the shell, 
    execute it with a file containing the directional numbers
    The one used here is the generator provided by Joe and Kuo [1] of which 
    the technical detail can be found in [2] and [3].

    **Reference:**

    (1) Frances Y. Kuo, Sobol Sequence Generator [C++ Source Code], Feb. 2015, 
        http://web.maths.unsw.edu.au/~fkuo/sobol
    (2) S. Joe and F.Y. Kuo, "Remark on Algorithm 659: Implementing Sobol's 
        quasirandom sequence generator," ACM Trans. Math. Softw. 29, 49-57 
        (2003)
    (3) S. Joe and F.Y. Kuo, "Constructing Sobol sequences with better 
        two-dimensional projections," SIAM J. Sci. Comput. 30, 2635-2654 (2008)

    :param n: (int) The length of the sequence
    :param d: (int) The dimension of the sequence
    :param generator: (str) The executable fullname of the generator
    :param dirnumfile: (str) The filename containing the directional numbers
    :returns: (ndarray) A numpy array containing the d-dimensional Sobol 
        sequence of length n
    """
    
    cmd = [generator, str(n), str(d), dirnumfile]  

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()

    # stdout in subprocess produces byte codes
    # convert to string and split into array with newline as separator
    sobol_seq = out.decode("utf-8").split("\n")

    # Remove the last two newlines
    sobol_seq.pop(-1)
    sobol_seq.pop(-1)

    # Convert the string into float
    for i in range(len(sobol_seq)):
        sobol_seq[i] = [float(_) for _ in sobol_seq[i].split()]

    # Convert to numpy array
    sobol_seq = np.array(sobol_seq)

    return sobol_seq

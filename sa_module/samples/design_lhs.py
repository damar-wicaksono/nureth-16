"""Module to generate (n-by-d) Latin Hypercube samples
"""
import numpy as np


def create(n, d, seed):
    r"""Generate :math:`(n \times d)` samples using Latin Hypercube sampling

    The function returns a numpy array of n-rows and d-dimensions filled with 
    randomly generated number from uniform distribution with support of [0, 1].
    The sampling is done in stratified manner to ensure that each 1/n interval 
    is represented by the samples, see [1] for additional detail.

    **Reference**:

    (1) Michael D. McKay, "Latin Hypercube Sampling as a Tool in Uncertainty 
        Analysis of Computer Models," In. Proc. of the 1992 Winter Simulation 
        Conference, Virginia, USA, 13-16 Dec. 1992.

    :param n: (int) The number of samples
    :param d: (int) The number of dimension
    :param seed: (int) The random seed number
    :returns: (ndArray) A numpy array of n-by-d filled with uniformly 
        generated random numbers in stratified manner
    """

    np.random.seed(seed)

    lhs = np.empty([n, d])

    for j in range(d):
        for i in range(n):
            lhs[i, j] = np.random.uniform(low=i/n, high=(i+1)/n)
        
        np.random.shuffle(lhs[:, j])

    return lhs

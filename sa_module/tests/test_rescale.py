# -*- coding: utf-8 -*
"""Module to test the rescale module"""
__author__ = "WD41"

import unittest

import env
from samples import rescale

class TestUnif(unittest.TestCase):
    """Test the uniform() function"""

    def setUp(self):
        """Test fixture build."""

        self.min_5_15 = 5.
        self.max_5_15 = 15.
        self.half_5_15 = 10.

    def test_unif_5_15(self):
        """Test uniform() for min. = 5. and max. = 15."""

        self.assertEqual(rescale.uniform(0.0, 5., 15), self.min_5_15) 
        self.assertEqual(rescale.uniform(1.0, 5., 15), self.max_5_15) 
        self.assertEqual(rescale.uniform(0.5, 5., 15), self.half_5_15) 

    def test_unif_type(self):
        """Test uniform() for type error."""

        with self.assertRaises(TypeError):
            rescale.uniform(0.5, 5., "15")

    def test_unif_high_quantile(self):
        """Test uniform() for the bound of quantile [0, 1]"""
        with self.assertRaises(ValueError):
            rescale.uniform(20, 5., 10.)

        with self.assertRaises(ValueError):
            rescale.uniform(-1, 5., 10.)

    def test_unif_min_max(self):
        """Test uniform() for the bound of min. and max. value"""
        with self.assertRaises(ValueError):
            rescale.uniform(0, 10, 10)
            
        with self.assertRaises(ValueError):
            rescale.uniform(0, 20, 10)        

class TestLogUnif(unittest.TestCase):
    """Test the loguniform() function"""
    
    def setUp(self):
        """Test fixture build"""
        
        self.min_10_1000 = 10.
        self.max_10_1000 = 1000.
        self.half_10_1000 = 100.
    
    def test_logunif_10_1000(self):
        """Test loguniform() for min. = 10. and max. 1000.
        Comparing float, used assertAlmostEqual
        """
        
        self.assertAlmostEqual(
            rescale.loguniform(0., 10., 1000.), self.min_10_1000
            )
        self.assertAlmostEqual(
            rescale.loguniform(1., 10., 1000.), self.max_10_1000
            )
        self.assertAlmostEqual(
            rescale.loguniform(0.5, 10., 1000.), self.half_10_1000
            )
    
    def test_logunif_type(self):
        """Test loguniform() for type error."""

        with self.assertRaises(TypeError):
            rescale.loguniform(0.5, 10., "1000")

    def test_logunif_high_quantile(self):
        """Test loguniform() for the bound of quantile [0, 1]"""
        with self.assertRaises(ValueError):
            rescale.loguniform(20, 10., 1000.)

        with self.assertRaises(ValueError):
            rescale.loguniform(-1, 10., 1000.)

    def test_logunif_min_max(self):
        """Test loguniform() for the bound of min. and max. value"""
        with self.assertRaises(ValueError):
            rescale.loguniform(0., 1000., 1000.)
            
        with self.assertRaises(ValueError):
            rescale.loguniform(0., 10000, 1000)        

class TestStdNormal(unittest.TestCase):
    """The the normal() function with default setting"""
    
    def setUp(self):
        """Test fixture build. Values were taken from R"""
        self.min_0_1 = -4.753424 # 1E-12
        self.max_0_1 =  4.753424 # 1-1E-12
        self.half_0_1 = 0.0 
        
    def test_stdnormal(self):
        """Test standard normal()"""
        
        self.assertEqual(
            round(rescale.normal(1e-6, [1e-18, 1-1e-18]), 6), self.min_0_1
            )
            
        self.assertEqual(
            round(rescale.normal(1-1e-6, [1e-18, 1-1e-18]), 6), self.max_0_1
            )
        
        self.assertEqual(
            round(rescale.normal(0.5, [1e-18, 1-1e-18]), 6), self.half_0_1
            )
    
    def test_stdnormal_type(self):
        """Test normal() for type error."""

        with self.assertRaises(TypeError):
            rescale.normal("1000", [1e-12, 1-1e-12])

    def test_stdnormal_high_quantile(self):
        """Test normal() for the bound of quantile [0, 1]"""
        with self.assertRaises(ValueError):
            rescale.normal(10, [1e-12, 1-1e-12])

        with self.assertRaises(ValueError):
            rescale.normal(-1, [1e-12, 1-1e-12])

    def test_stdnormal_min_max(self):
        """Test normal() for the bound of min. and max. value"""
        with self.assertRaises(ValueError):
            rescale.normal(0.0, [1-1e-12, 1-1e-12])
            
        with self.assertRaises(ValueError):
            rescale.normal(0.0, [1-1e-12, 1e-12])

class TestNormal(unittest.TestCase):
    """The the normal() function with mean = 10.0 and variance = 9.0"""
    
    def setUp(self):
        """Test fixture build. Values were taken from R"""
        self.min_10_9 = -4.260273
        self.max_10_9 =  24.26027
        self.half_10_9 = 10.0
        
    def test_stdnormal(self):
        """Test standard normal()"""
        
        self.assertEqual(
            round(rescale.normal(1e-6, [1e-18, 1-1e-18], 10., 9.), 6), 
            self.min_10_9
            )
            
        self.assertEqual(
            round(rescale.normal(1-1e-6, [1e-18, 1-1e-18], 10., 9.), 5), 
            self.max_10_9
            )
        
        self.assertEqual(
            round(rescale.normal(0.5, [1e-18, 1-1e-18], 10., 9.), 5), 
            self.half_10_9
            )
    
    def test_normal_type(self):
        """Test normal() for type error."""

        with self.assertRaises(TypeError):
            rescale.normal(0.1, [1e-18, 1-1e-18], "10", "9")

    def test_normal_var(self):
        """Test normal() for the positive value of variance"""
        with self.assertRaises(ValueError):
            rescale.normal(0.0, [1-1e-18, 1-1e-18], 10., -9.)
            

if __name__ == "__main__":
    unittest.main(verbosity=2)

# -*- coding: utf-8 -*-
"""Module to unit test the Ishigami test function"""
__author__ = "WD41"

import unittest

import env
from test_functions import ishigami


class TestIshigami(unittest.TestCase):
    """Test the ishigami.evaluate() function"""

    def setUp(self):
        """Test fixture build"""
        #x1 = 0.5 * pi
        #x2 = 0.15 * pi
        #x3 = 0.25 * pi
        #x4 = 7
        #x5 = 0.1
        self.ref_value = 2.480802 # Calculated in Excel
      
    def test_ishigami_ref(self):
        """Test the Ishigami function against a reference value"""
        import numpy as np

        x1 = 0.5 * np.pi
        x2 = 0.15 * np.pi
        x3 = 0.25 * np.pi
        x4 = 7
        x5 = 0.1

        self.assertAlmostEqual(
            ishigami.evaluate(np.array([x1, x2, x3]), np.array([x4, x5])), 
            self.ref_value
        )

    def test_ishigami_list(self):
        """Test the Ishigami function using list as argument value"""
        from numpy import pi
        x1 = 0.5 * pi
        x2 = 0.15 * pi
        x3 = 0.25 * pi
        x4 = 7
        x5 = 0.1

        self.assertAlmostEqual(
            ishigami.evaluate([x1, x2, x3], [x4, x5]), self.ref_value
        )

    def test_ishigami_type(self):
        """Test the Ishigami function for type error"""
        
        with self.assertRaises(TypeError):
           ishigami.evaluate(["c", "a", "21"], [7, 0.1])

if __name__ == "__main__":
    unittest.main(verbosity=2)

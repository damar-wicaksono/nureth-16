# -*- coding: utf-8 -*-
"""sobol_saltelly.py: module with function to generate Sobol-Saltelli Design
The design is used for Monte Carlo simulation to estimate the Sobol sensitivity
indices
"""
import numpy as np
from .. import samples


def create(n, k, seed, scheme="srs"):
    """ Generate Sobol-Saltelli design matrix

    Sobol-Saltelli design matrix calculate the Sobol sensitivity indices using 
    Monte Carlo simulation by sampling-resampling scheme as proposed in [1].
    The design requires n*(k+2) code runs for first-order and total sensitivity 
    indices and n*(2*k+2) code runs for additional second order sentivity 
    indices.

    Jansen's Estimator [2] is used to calculate the first and total sensitivity 
    indices and Saltelli estimator [3] is used to calculate the second-order 
    indices.

    **References:**

    (1) Andrea Saltelli, et al., "Variance based sensitivity analysis of model 
        output. Design and estimator for the total sensitivity index," Computer 
        Physics Communications, 181, pp. 259-270, (2010)
    (2) Andrea Saltelli, "Making best use of model evaluations to compute 
        sensitivity indices," Computer Physics Communications 145, pp. 280-297, 
        (2002)

    :param n: (int) The number of samples
    :param k: (int) The number of model parameters
    :param seed: (int) The seed number for "srs" and "lhs" 
        (array of str) The fullname of generator and file containing direction 
        number for "sobol"
    :param scheme: (str) The scheme to generate sampled points
    :returns : (dict of ndArray) A dictionary containing pair of key and a 
        numpy array of which the row correspond to parameter values for model 
        evaluation
    """
    # Generate samples 2 times in dimension for "sample" and "resample"
    if scheme == "srs":
        ab = samples.design_srs.create(n, 2*k, seed)
    if scheme == "lhs":
        ab = samples.design_lhs.create(n, 2*k, seed)
    if scheme == "sobol":
        ab = samples.design_sobol.create(n, 2*k, seed[0], seed[1])

    a = ab[:, 0:k]
    b = ab[:, k:2*k]
    sobol_saltelli = dict()
    sobol_saltelli["a"] = a
    sobol_saltelli["b"] = b

    # AB_i: replace the i-th column of A matrix by the i-th column of B matrix
    # These sets of samples are used to calculate the first- and total-order 
    # sensitivity indices.
    for i in range(k):
        key = "ab_{}" .format(str(i+1))
        temp = np.copy(a)
        temp[:, i] = b[:, i]
        sobol_saltelli[key] = temp
    
    # If second-order indices is to be calculated then additional samples
    # are required, make it always available.
    # Make it for the evaluation and analyzing function to decide which indices
    # to calculate.
    for i in range(k):
        key = "ba_{}" .format(str(i+1))
        temp = np.copy(b)
        temp[:, i] = a[:, i]
        sobol_saltelli[key] = temp
    
    return sobol_saltelli


def write(sobol_saltelli_dict, fullname, format="%1.6e"):
    """Write the dictionary of Sobol-Saltelli design matrix into a file
    
    The matrix is written in group such that a group of adjacent rows belongs 
    to one MC sample need to calculate all the indices.
    This way, better distribution can be achieved when jobs are sent in batch.
    
    The convention used in this implementation is for each of the n samples,
    (2*k+2) code runs are required to obtain the first-order, second-order, 
    and the total-order sensitivity indices. For block i of n each with 
    (2*k+2) rows:
    
        1. (A)j (row i of matrix A, j=1,...,n)
        2. (B)j (row i of matrix B, j=1,...,n)
        3. (AB_i)j (row j of matrix AB_i where column i-th of A changed by B,
            j=1,...,n, i=1,...,k)
        4. (BA_i)j (row j of matrix BA_i where column i-th of B changed by A,
            j=1,...,n, i=1,...k)
    
    :param sobol_saltelli_dict: (dict) The dictionary of the design matrix
    :param fullname: (str) The fullname (incl. path) for saving the matrix
    :param format: (str) The print format for the number
    """
    # Infer the number of samples and parameters inside the matrix
    n = sobol_saltelli_dict["a"].shape[0]
    k = sobol_saltelli_dict["a"].shape[1]
    total_runs = n * (2 * k + 2)
    
    sobol_saltelli_np = np.empty([0, k])
    for i in range(n):
        sobol_saltelli_np = np.vstack(
            (sobol_saltelli_np, sobol_saltelli_dict["a"][i, :])
        )
        sobol_saltelli_np = np.vstack(
            (sobol_saltelli_np, sobol_saltelli_dict["b"][i, :])
        )
        for j in range(k):
            key = "ab_{}" .format(str(j+1))
            sobol_saltelli_np = np.vstack(
                (sobol_saltelli_np, sobol_saltelli_dict[key][i, :])
            )
        for j in range(k):
            key = "ba_{}" .format(str(j+1))
            sobol_saltelli_np = np.vstack(
                (sobol_saltelli_np, sobol_saltelli_dict[key][i, :])
           )
                
    np.savetxt(fullname, sobol_saltelli_np, fmt=format)


def read(y_big, n, k):
    r"""Generate python dict. of model output according to the sample-resample

    The function will read a big array (:math:`n \times (k + 2) for first- and 
    total-order or :math:`n \times (2k + 2)` with additional second-order 
    indices calculations) of model output and rearrange them into a python 
    dictionary according to the sample-resample scheme.

    Example of generated keys: "ab_1" the output of model evaluations using 
    matrix A with i-th column exchange from the matrix B, etc.

    :param y_big: (ndarray) A numpy array with all model outputs
    :param n: (int) The number of samples (not total runs)
    :param k: (int) The number of model parameters
    :returns: (dict) a dictionary with rearranged output
    """
    # The number of total runs
    n_big = y_big.shape[0]

    # Split the arrays into multiple arrays and assign a key for it to be a 
    # dictionary
    y = dict()
    
    # Check if total runs correspond to 2nd-order indices computation
    # TODO: this check is supressed for later version, always assume 1st order
    #if ((n_big//n)-2) // k == 2:
    #    a = 2
        # If 2nd-order indices were calculated iterate over BA matrix
    #    for i in range(k):
    #        key = "ba_{}" .format(str(i+1))
    #        y[key] = y_big[[(j * (a*k+2) + i + k + 2) for j in range(n)]]
    #else:
    a = 1

    y["a"] = y_big[[(i * (a*k+2)) for i in range(n)]]
    y["b"] = y_big[[(i * (a*k+2) + 1) for i in range(n)]]
    
    # Iterate over AB matrix
    for i in range(k):
        key = "ab_{}" .format(str(i+1))
        y[key] = y_big[[(j * (a*k+2) + i + 2) for j in range(n)]]

    return y


def create_list_runs(n, k, indices=1):
    r"""Create a list to be iterated refering to model runs to compute indices.

    The read() function defined in this module was ordered in a block of 
    Sobol-Saltelli samples such that for each of a sequential (k+2) runs it 
    belongs to the same Sobol sample necessary to compute the 1st- and 
    total-order indices. 

    If only second order indices are computed then this function returns only 
    sequence of :math:`n \times k` additional runs which is required. 
    
    If all indices are required then the function returns trivial value of all 
    the necessary runs (:math:`n \times (2 \times k + 2)`)

    :param n: (int) The total number of Sobol-Saltelli samples
    :param k: (int) The total number of model parameters
    :param indices: (int) The list to generate a list of required runs for 
        the computation of either 1st-, 2nd-, or both indices
    :returns: (list of int) The list of runs, one-indexed
    """
    list_runs = []
    for i in range(n):
        list_runs_i = []
        if indices == 1:
            shift = i * (2 * k + 2) + 1
            for j in range(k+2):
                list_runs_i.append(shift+j)
            list_runs.append(list_runs_i)
        
        elif indices == 2:
            shift = i * (2 * k + 2) + (k + 2) + 1
            for j in range(k):
                list_runs_i.append(shift+j)
            list_runs.append(list_runs_i)

        elif indices == 3:
            shift = i * (2 * k + 2) + 1
            for j in range(2*k+2):
                list_runs_i.append(shift+j)
            list_runs.append(list_runs_i)
    
    return list_runs 

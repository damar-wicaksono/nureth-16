# -*- coding: utf-8 -*-
"""indices.py: module with function to calculate sensitivity indices based on 
the Sobol-Saltelli design matrix.
The design is used for Monte Carlo simulation to estimate the Sobol sensitivity
indices
"""
import numpy as np
from .. import samples


def evaluate_1st(
    y_dict, bootstrap=True, bootstrap_seed=20151418, bootstrap_n=10000
    ):
    r"""Calculate the 1st-order and Total-order Sobol Sensitivity Indices

    The model output dictionary was created by using the read() function. 
    
    First order indices are calculated using Saltelli's Estimator and Total 
    order indices are calculated using Jansen's Estimator. 
    See [1] for further detail.

    The uncertainty of the estimator is assessed using bootstrap analysis. 
    The size of each bootstrap sample used the same number of samples to 
    estimate the indices. 
    Two bootstrap confidence interval is used, one is +- 1.96 SEM and the other 
    is based on percentile with 95% coverage.
    Detail on the bootstrap analysis can be found in [2].

    **References:**

    (1) A. Saltelli, et al., "Variance based sensitivity analysis of model 
        output. Design and estimator for the total sensitivity index,"
        Computer Physics Communications, 181, pp. 259-270, 2010.
    (2) G.E.B. Archer, A. Saltelli, and I.M. Sobol, "Sensitivity Measures, 
        ANOVA-like Techniques and the Use of Bootstrap," J. Statist. Comput. 
        Simul., vol. 58, pp. 99-120, 1997.

    :param y_dict: (dict) The model output in dictionary
    :param bootstrap_seed: (int) Random seed number for bootstrap samples
    :param bootstrap_n: (int, optional) The number of bootstrap samples.
    :returns: (2 dicts) Dictionary of 1st-order and total-order sensitivity
        indices along with the confidence interval found from bootstrapping and 
        another dict containing numpy arrays of each indices from bootstrap 
        samples
    """
    mean = np.mean(y_dict["a"] * y_dict["b"])
    var = np.var(y_dict["a"], ddof=1)

    if "ba_1" in y_dict.keys():
        k = int((len(y_dict.keys()) - 2) / 2)
    else:
        k = int(len(y_dict.keys()) - 2)

    n = y_dict["a"].shape[0]

    si = dict()
    # Calculate the first-order and total-order indices
    for i in range(1, k+1):
        key1 = "ab_{}" .format(i)
        key2 = "S{}" .format(i)
        key3 = "ST{}" .format(i)
        si[key2] = (np.mean(y_dict[key1] * y_dict["b"]) - mean) / var
        si[key3] = 0.5 * np.mean((y_dict["a"] - y_dict[key1])**2) / var

    # Calculate the bootstrap
    if bootstrap:
        np.random.seed(bootstrap_seed)
        si_boot = dict()
        for i in range(1, k+1):
            key1 = "S{}" .format(i)
            key2 = "ST{}" .format(i)
            si_boot[key1] = np.empty([0])
            si_boot[key2] = np.empty([0])

        for i in range(bootstrap_n):
            # Sample all the original samples with replacement
            boot_ix = np.random.randint(0, high=n, size=(n))
            mean = np.mean(y_dict["b"][boot_ix] * y_dict["a"][boot_ix])
            var = np.var(y_dict["a"][boot_ix], ddof=1)
            for j in range(1, k+1):
                key1 = "ab_{}" .format(j)
                key2 = "S{}" .format(j)
                key3 = "ST{}" .format(j)
                temp = (np.mean(
                    y_dict[key1][boot_ix] * y_dict["b"][boot_ix]) - mean) / var 
                si_boot[key2] = np.append(si_boot[key2], temp)
                temp = 0.5 * \
                    np.mean((y_dict["a"][boot_ix] - y_dict[key1][boot_ix])**2)\
                    / var

                si_boot[key3] = np.append(si_boot[key3], temp)

        # Calculate the bootstrap confidence interval
        for i in range(1, k+1):
            key1 = "S{}" .format(i)
            key2 = "S{}_CI_SE" .format(i)
            key3 = "S{}_CI_%" .format(i)
            se = np.std(si_boot[key1], ddof=1)
            si[key2] = [si[key1] - 1.96 * se, si[key1] + 1.96 * se]
            si[key3] = [
                np.percentile(si_boot[key1], 2.5), 
                np.percentile(si_boot[key1], 97.5)
                ]

        for i in range(1, k+1):
            key1 = "ST{}" .format(i)
            key2 = "ST{}_CI_SE" .format(i)
            key3 = "ST{}_CI_%" .format(i)
            se = np.std(si_boot[key1], ddof=1)
            si[key2] = [si[key1] - 1.96 * se, si[key1] + 1.96 * se]
            si[key3] = [
                np.percentile(si_boot[key1], 2.5), 
                np.percentile(si_boot[key1], 97.5)
                ]

    if bootstrap:
        return si, si_boot
    else:
        return si

def evaluate_1st_janon(
    y_dict, bootstrap=True, bootstrap_seed=20151418, bootstrap_n=10000
    ):
    r"""Calculate the 1st-order and Total-order Sobol Sensitivity Indices

    The model output dictionary was created by using the read() function. 
    
    First order indices are calculated using Saltelli's Estimator and Total 
    order indices are calculated using Jansen's Estimator. 
    See [1] for further detail.

    The uncertainty of the estimator is assessed using bootstrap analysis. 
    The size of each bootstrap sample used the same number of samples to 
    estimate the indices. 
    Two bootstrap confidence interval is used, one is +- 1.96 SEM and the other 
    is based on percentile with 95% coverage.
    Detail on the bootstrap analysis can be found in [2].

    **References:**

    (1) A. Saltelli, et al., "Variance based sensitivity analysis of model 
        output. Design and estimator for the total sensitivity index,"
        Computer Physics Communications, 181, pp. 259-270, 2010.
    (2) G.E.B. Archer, A. Saltelli, and I.M. Sobol, "Sensitivity Measures, 
        ANOVA-like Techniques and the Use of Bootstrap," J. Statist. Comput. 
        Simul., vol. 58, pp. 99-120, 1997.

    :param y_dict: (dict) The model output in dictionary
    :param bootstrap_seed: (int) Random seed number for bootstrap samples
    :param bootstrap_n: (int, optional) The number of bootstrap samples.
    :returns: (2 dicts) Dictionary of 1st-order and total-order sensitivity
        indices along with the confidence interval found from bootstrapping and 
        another dict containing numpy arrays of each indices from bootstrap 
        samples
    """
    mean = np.mean(y_dict["a"] * y_dict["b"])
    var = np.var(y_dict["a"], ddof=1)

    if "ba_1" in y_dict.keys():
        k = int((len(y_dict.keys()) - 2) / 2)
    else:
        k = int(len(y_dict.keys()) - 2)

    n = y_dict["a"].shape[0]

    si = dict()
    # Calculate the first-order and total-order indices
    for i in range(1, k+1):
        key1 = "ab_{}" .format(i)
        key2 = "S{}" .format(i)
        key3 = "ST{}" .format(i)
#        si[key2] = (np.mean(y_dict[key1] * y_dict["b"]) - mean) / var
# Use Janon's (et al.) estimator:
#        si[key2] = np.mean(y_dict[key1] * y_dict["b"]) - (np.mean((y_dict[key1] + y_dict["b"])/2))**2
#        temp = np.mean((y_dict["b"]**2 + y_dict[key1]**2)/2) - (np.mean((y_dict["b"] + y_dict[key1])/2))**2
        si[key2] = janon_1st(y_dict["b"], y_dict[key1])
# Janon's estimator stops here
        si[key3] = 0.5 * np.mean((y_dict["a"] - y_dict[key1])**2) / var

    # Calculate the bootstrap
    if bootstrap:
        np.random.seed(bootstrap_seed)
        si_boot = dict()
        for i in range(1, k+1):
            key1 = "S{}" .format(i)
            key2 = "ST{}" .format(i)
            si_boot[key1] = np.empty([0])
            si_boot[key2] = np.empty([0])

        for i in range(bootstrap_n):
            # Sample all the original samples with replacement
            boot_ix = np.random.randint(0, high=n, size=(n))
            mean = np.mean(y_dict["b"][boot_ix] * y_dict["a"][boot_ix])
            var = np.var(y_dict["a"][boot_ix], ddof=1)
            for j in range(1, k+1):
                key1 = "ab_{}" .format(j)
                key2 = "S{}" .format(j)
                key3 = "ST{}" .format(j)
#                temp = (np.mean(
#                    y_dict[key1][boot_ix] * y_dict["b"][boot_ix]) - mean) / var 
# Janon's estimator
#                temp1 = (np.mean((y_dict[key1][boot_ix] + y_dict["b"][boot_ix])/2))**2
#                temp2 = np.mean(y_dict[key1][boot_ix] * y_dict["b"][boot_ix])
#                temp3 = np.mean((y_dict[key1][boot_ix]**2 + y_dict["b"][boot_ix]**2)/2)
                temp  = janon_1st(y_dict["b"][boot_ix], y_dict[key1][boot_ix])
                si_boot[key2] = np.append(si_boot[key2], temp)
# End of Janon estimator implementation
                temp = 0.5 * \
                    np.mean((y_dict["a"][boot_ix] - y_dict[key1][boot_ix])**2)\
                    / var

                si_boot[key3] = np.append(si_boot[key3], temp)

        # Calculate the bootstrap confidence interval
        for i in range(1, k+1):
            key1 = "S{}" .format(i)
            key2 = "S{}_CI_SE" .format(i)
            key3 = "S{}_CI_%" .format(i)
            se = np.std(si_boot[key1], ddof=1)
            si[key2] = [si[key1] - 1.96 * se, si[key1] + 1.96 * se]
            si[key3] = [
                np.percentile(si_boot[key1], 2.5), 
                np.percentile(si_boot[key1], 97.5)
                ]

        for i in range(1, k+1):
            key1 = "ST{}" .format(i)
            key2 = "ST{}_CI_SE" .format(i)
            key3 = "ST{}_CI_%" .format(i)
            se = np.std(si_boot[key1], ddof=1)
            si[key2] = [si[key1] - 1.96 * se, si[key1] + 1.96 * se]
            si[key3] = [
                np.percentile(si_boot[key1], 2.5), 
                np.percentile(si_boot[key1], 97.5)
                ]

    if bootstrap:
        return si, si_boot
    else:
        return si

def janon_1st(f_b, f_ab_i):
    """Calculate the Sobol 1st order indices using the Janon's estimator
    
    This is a direct numpy implementation of Janon's second estimator given
    by equation (6) in pp.4 on [1].

    *Reference*:
    (1) A. Janon, et al., "Asymptotic normality and efficiency of two Sobol 
        index estimators," ESAIM: Probability and Statistics, EDP Sciences, 
        2003.

    :param f_b: (ndArray) a numpy array with model output from the B matrix
    :param f_ab_i: (ndArray) a numpy array with model output from the A matrix
        but with the i-th column substituted by the i-th column of the B matrix
    :returns: (float) the Sobol 1st-order sensitivity estimates of parameter i
    """
    mean_square = (np.mean((f_b + f_ab_i)/2))**2
    nominator = np.mean(f_b * f_ab_i) - mean_square
    denominator = np.mean((f_b**2 + f_ab_i**2)/2) - mean_square

    janon_1st = nominator / denominator

    return janon_1st

def analyze_2nd(y_dict, sa_indices_dict, bootstrap_seed, bootstrap_n=10000):
    """Calculate the second order Sobol sensitivity indices from model output
    
    Calculation is based on the Saltelli's paper where using the same output 
    will yield 2 estimation of the 2nd-order sensitivity indices. 
    See [1] for further detail.

    **Reference:**

    (1) A. Saltelli, "Making best use of model evaluations to compute 
        sensitivity indices," Computer Physics Communications, 145, pp.280-297,
        2002.

    :param y_dict: (dict)
    :param sa_indices_dict: (dict)
    :param bootstrap_seed: (int)
    :param bootstrap_n: (int)
    :returns: (dict) Indices dictionary updated with 2 estimations of the 2nd-
        order indices along with their bootstrap confidence interval
    """
    return sa_indices_dict

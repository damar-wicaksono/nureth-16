__author__ = 'wicaksono_d'

import argparse

def get_input_feba():
    """Parse input argument for Morris Screening Method

    :return: case (string), which experimental test run to simulate.
        num_trajectories (int), Number of trajectories in Morris screening 
        method. num_levels (int), number of levels in Morris design, even 
        number value is recommended. delta (float), the grid jump size
    """

    import argparse

    parser = argparse.ArgumentParser(
        description="Execute TRACE to simulate selected FEBA "\
                    "transient for Morris Screening Analysis"
    )

    # Select which test number to analyze
    parser.add_argument(
        "-c", "--case",
        type=str,
        choices=["214", "216", "218", "220", "222", "223"],
        help="FEBA Cases to Run",
        required=True
    )

    # specify the number of trajectories
    parser.add_argument(
        "-nr", "--trajectories",
        type=int,
        default=10,
        help="How many trajectories to run",
        required=False
    )

    # specify the number of levels
    parser.add_argument(
        "-nq", "--levels",
        type=int,
        default=4,
        help="how many levels of OAT, choose even number please",
        required=False
    )

    # specify the number of processors
    parser.add_argument(
        "-np", "--numprocs",
        type=int,
        default=1,
        help="Number of processors to use",
        required=False
    )

    # specify the seed number for random sampling
    parser.add_argument(
        "-ns", "--numseeds",
        nargs=2,
        type=int,
        help="Set 2 random seed number (for reproducibility)",
        required=True
    )

    args = parser.parse_args()

    return args.case, args.trajectories, args.levels, args.numseeds, \
           args.numprocs

def sobol():
    """Parse input argument for Sobol sensitivity analysis

    :return: case (string), which experimental test run to simulate.
        num_trajectories (int), Number of trajectories in Morris screening 
        method. num_levels (int), number of levels in Morris design, even 
        number value is recommended. delta (float), the grid jump size
    """

    import argparse

    parser = argparse.ArgumentParser(
        description="Execute TRACE to simulate selected FEBA "\
                    "transient for Sobol sensitivity analysis"
    )

    # Select which test number to analyze
    parser.add_argument(
        "-c", "--case",
        type=str,
        choices=["214", "216", "218", "220", "222", "223"],
        help="FEBA Cases to Run",
        required=True
    )

    # specify the location of design matrix file
    parser.add_argument(
        "-d", "--design",
        type=str,
        help="Which design matrix file to use",
        required=True
    )

    # specify the range of samples to be calculated
    parser.add_argument(
        "-ns", "--numsamples",
        nargs=2,
        type=int,
        help="Set 2 range of samples",
        required=True
    )

    # specify the number of processors
    parser.add_argument(
        "-np", "--numprocs",
        type=int,
        default=1,
        help="Number of processors to use",
        required=False
    )

    # specify whether calculate second order indices
    parser.add_argument(
        "--interact",
        dest='interact',
        action='store_true'
    )
    args = parser.parse_args()

    return args.case, args.design, args.numsamples, args.numprocs, args.interact

def get_input_analyze():
    """Get parameters from command line arguments for Morris analysis routines

    :return: (int) case number and (string) morris design matrix filename
    """
    parser = argparse.ArgumentParser(
        description="Analyze FEBA simulation for Morris Screening Analysis."
    )

    # Select which case to analyze
    parser.add_argument(
        "-c", "--case",
        type=str,
        choices=["214", "216", "218", "220", "222", "223"],
        help="FEBA test cases to run",
        required=True
    )

    # Specify the Scaled Morris design matrix filename.
    # Standardized Elementary Effect requires the scaled one.
    parser.add_argument(
        "-md", "--morrisfile",
        type=str,
        help="Specify Morris design matrix file",
        required=True
    )

    args = parser.parse_args()

    return args.case, args.morrisfile

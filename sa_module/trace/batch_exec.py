__author__ = 'wicaksono_d'

def batch_run(num_samples, num_processors):
    """Create a list of iterator in batch size.

    The batch size is depending on the number of processors.
    Batch runs are required to maximize processor occupancy

    **References:**
    Taken from
    http://code.activestate.com/recipes/303279-getting-items-in-batches/

    :param num_samples: (int) number of samples to be run
    :param num_processors: (int) number of available processors, the size 
        of batch
    :returns: (iterator) an iterator in batch size
    """
    import itertools

    iterable = range(0, num_samples)
    source_iter = iter(iterable)
    while True:
        batch_iter = itertools.islice(source_iter, num_processors)
        yield itertools.chain([next(batch_iter)], batch_iter)
		
def exec_trace(logfiles, dirnames, scratch_dirnames, trc_cmds, xtv_cmds,
               max_task, smpl_nums, test_no='-'):
    """Execute multiple TRACE runs

    Execute multiple TRACE runs by passing a list of trace terminal commands.
    Timeout (forced kill if it takes too long) was set to 8'000 [s].

    :param logfiles: (list of str) list of logfiles fullname
    :param dirnames: (list of str) list of running directory names
    :param scratch_dirnames: (list of str) list of directory names in scratch
    :param trc_cmds: (list of str) command to execute TRACE
    :param xtv_cmds: (list of str) command to convert xtv to dmx
    :param max_task: (int) The number of processor can be used at a given time
    :param smpl_nums:(str) Run number for terminal report
    :param test_no: (str) FEBA test number for terminal report
    """
    import subprocess
    import time

    if not trc_cmds:
        return

    def done(p):
        return p.poll() is not None
    def success(p):
        return p.returncode == 0
    def fail(sample_num):
        print('FEBA Test No. {} on sample {} is killed due to TimeOutError'\
              '(it takes too goddamn long).' .format(test_no, sample_num))

    processes = []
    n = 0
    k = 0

    while True:
        while trc_cmds and len(processes) < max_task:
            task = trc_cmds.pop(0)
            logfile = logfiles.pop(0)
            dirname = dirnames.pop(0)
            print('\n#################################')
            print('Executing TRACE on FEBA Test No. {} on sample {}.'\
                    .format(test_no, smpl_nums[n]))
            print(subprocess.list2cmdline(task))
            processes.append(subprocess.Popen(task, stdout=logfile, cwd=dirname))
            n += 1

        for l, p in enumerate(processes):
            try:
                p.wait(timeout=8000)
            except subprocess.TimeoutExpired:
                p.kill()
            if done(p):
                if success(p):
                    logfile.close()
                    processes.remove(p)
                else:
                    fail(smpl_nums[l])
                    processes.remove(p)


        if not processes and not trc_cmds:
            break
        else:
            time.sleep(0.05)

    processes = []
    while True:
        # Process XTV to DMX now
        while xtv_cmds and len(processes) < max_task:
            task = xtv_cmds.pop(0)
            scratch_dirname = scratch_dirnames.pop(0)
            print('\n#################################')
            print('Converting XTV to DMX of PERICLES Test No. {} on sample {}.'\
                    .format(test_no, smpl_nums[k]))
            print(subprocess.list2cmdline(task))
            processes.append(subprocess.Popen(task, cwd=scratch_dirname))
            k += 1

        for l, p in enumerate(processes):
            try:
                p.wait(timeout=1000)
            except subprocess.TimeoutExpired:
                p.kill()
            if done(p):
                if success(p):
                    logfile.close()
                    processes.remove(p)
                else:
                    fail(smpl_nums[l])
                    processes.remove(p)

        if not processes and not trc_cmds:
            break
        else:
            time.sleep(0.05)

#listinptodict.py: Process the listVars input file into python dictionary
__author__ = 'wicaksono_d'

def inpvars_to_dict(listvars_filename):
    """Read listVars input file and create a dictionary

    :param listvars_filename: (str) fullname of listVars input file
    :returns: (list of dictionaries) Contains the data read from listVars 
        input file
    """
    from .shared_data import sens_coefs, components

    inpvars_to_dict = []

    def parse_spacer_listvars(line, inpvars_dict):
        """
        Parse spacer data type specified in listVars input file
        """
        spacer_data = line.split()
        print('*{0}* component, gridid *{1}* and *{2}* variable specified.'
                .format(spacer_data[1], spacer_data[2], spacer_data[3]))
        spacer_dict = {
                'type': spacer_data[1],
                'num': spacer_data[2],
                'var_name': spacer_data[3],
                'var_type': spacer_data[4],
                'var_card': spacer_data[5],
                'var_word': spacer_data[6],
                'var_dist': spacer_data[7],
                'var_min' : spacer_data[8],
                'var_max' : spacer_data[9]
                }
        inpvars_dict.append(spacer_dict)
        return inpvars_dict

    def parse_matprop_listvars(line, inpvars_dict):
        """
        Parse material property data type specified in listVars input file.
        """
        matprop_data = line.split()
        print('Material number *{0}* and *{1}* property is specified' \
              .format(matprop_data[2], matprop_data[3]))

        matprop_dict = {
                'type': matprop_data[1],
                'num': matprop_data[2],
                'var_name': matprop_data[3],
                'var_dist': matprop_data[7],
                'var_min' : matprop_data[8],
                'var_max' : matprop_data[9]
                }
        if matprop_data[4] == 'table':
            # A Table-specified material properties
            matprop_dict['var_type'] = 'table'
            matprop_dict['var_col'] = matprop_data[5]  # ??? Which column
            matprop_dict['var_num'] = matprop_data[6]  # ??? How many
        elif matprop_data[4] == 'fit':
            # A Functional fit material properties
            matprop_dict['var_type'] = 'fit'
            #matprop_dict['var_card'] = matprop_data[5]
        else:
            raise SystemExit

        inpvars_dict.append(matprop_dict)
        return inpvars_dict

    def parse_comp_listvars(line, inpvars_dict):
        """
        Parse component data type specified in listVars input file.
        """
        comp_data = line.split()
        print('*{0}* component, number *{1}* and *{2}* variable is specified.'\
               .format(comp_data[1], comp_data[2], comp_data[3]))

        comp_dict = {
                'type': comp_data[1],
                'num': comp_data[2],
                'var_name': comp_data[3],
                'var_dist': comp_data[7],
                'var_min' : comp_data[8],
                'var_max' : comp_data[9]
                }

        if comp_data[4] == 'scalar':
            # A scalar type variable
            comp_dict['var_type'] = 'scalar'
            comp_dict['var_card'] = comp_data[5]
            comp_dict['var_word'] = comp_data[6]
        elif comp_data[4] == 'table':
            # An array type variable
            comp_dict['var_type'] = 'table'
            comp_dict['var_col'] = comp_data[5]
            comp_dict['var_num'] = comp_data[6]
        elif comp_data[4] == 'array':
            # An array type variable
            comp_dict['var_type'] = 'array'
            comp_dict['var_col'] = comp_data[5]
            comp_dict['var_num'] = comp_data[6]
        else:
            raise SystemExit

        inpvars_dict.append(comp_dict)
        return inpvars_dict

    def parse_sens_coef_listvars(line, inpvars_dict):
        """
        Parse component data type specified in listVars input file.
        """
        sens_coef_data = line.split()
        print('Sensitivity Coefficients ID *{}* specified.'\
                .format(sens_coef_data[2]))

        sens_coef_dict = {
                'type': sens_coef_data[1],
                'num': sens_coef_data[2],
                'var_type': sens_coef_data[4],
                'var_dist': sens_coef_data[7],
                'var_min' : sens_coef_data[8],
                'var_max' : sens_coef_data[9]
                }

        inpvars_dict.append(sens_coef_dict)
        return inpvars_dict

    with open(listvars_filename, 'rt') as listvars_file:
        for line in listvars_file.readlines():
            if not line.startswith('#'):

                # Spacer grid data is specified
                if 'spacer' in line:
                    inpvars_to_dict = parse_spacer_listvars(
                            line, inpvars_to_dict
                            )

                # Material property is specified
                elif 'matprop' in line:
                    inpvars_to_dict = parse_matprop_listvars(
                            line, inpvars_to_dict
                            )

                elif any([component in line for component in components]):
                    inpvars_to_dict = parse_comp_listvars(
                            line, inpvars_to_dict
                            )

                elif 'senscoef' in line:
                    inpvars_to_dict = parse_sens_coef_listvars(
                            line, inpvars_to_dict
                            )

                else:
                    print('*{}* component is not explicitly defined in the \
                            script! Sorry.' .format(line.split()[1]))
                    empty_dict = dict()
                    inpvars_to_dict.append(empty_dict)

        return inpvars_to_dict

def rescale_design(inp_vars, sample_datafilename):
    """Rescale the design specify in the normalized design file

    :param inp_vars: (list of dicts) the list of dictionaries to be updated 
        containing the type and parameter of the distributions.
    :param sample_datafilename: (str) fullname of the normalized design file.
    :returns: (numpy array) rescaled design matrix
    """
    import numpy as np
    from ..samples import rescale

    sampled_data = np.loadtxt(sample_datafilename)
    rescaled_sampled_data = np.zeros(sampled_data.shape, dtype=float)

    for i in range(sampled_data.shape[0]):
        for j, param in zip(range(sampled_data.shape[1]), inp_vars):
            if param['var_dist'] == 'unif':
                rescaled_sampled_data[i][j] = rescale.uniform(
                        sampled_data[i][j], float(param['var_min']),
                        float(param['var_max'])
                        )
            elif param['var_dist'] == 'logunif':
                rescaled_sampled_data[i][j] = rescale.loguniform(
                        sampled_data[i][j], float(param['var_min']),
                        float(param['var_max'])
                        )
            else:
                print("{} distribution not supported yet"\
                        .format(param['var_dist']))

    return rescaled_sampled_data

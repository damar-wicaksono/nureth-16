# List of explicitly supported component type
components = ['pipe', 'vessel', 'power', 'fill', 'break']

# List of explicitly supported sensitivity coefficient
sens_coefs = []
for i in range(1000, 1032): # ??? There are 31 Sens Coefs with ID 1000-1031
    sens_coefs.append(str(i))

# key for material properties
matprop_tracin_key = 'User Defined Material'

fit_table_keys = {
        'rho': 'density functional fit',
        'cond': 'thermal conductivity functional fit',
        'cp': 'specific heat fit',
        'emis': 'emissivity functional fit'
        }

__author__ = 'wicaksono_d'

def write_template(input_vars, tracin_filename, template_filename):
    """Write TRACE input template file

    Template file is a trace input file of which the selected parameters has
    been substituted with a key.
    This key will later again be substituted with value taken from a sample
    data file.

    :param input_vars: (list of dictionary) python data structure for 
        parameters read from listVars input file.
    :param tracin_filename: (str) filename of TRACE input file, include the 
        path. Note also that TRACE input file and listVars input file come in 
        pair, i.e., they are dependent.
    :param template_filename: (str) filename of template file, include the path
    :returns: a template text file with filename template_filename.
    """
    import re
    from .shared_data import components, sens_coefs

    with open(tracin_filename, 'rt') as tracin_file:
        tracin_lines = tracin_file.read().splitlines()

    def parse_spacer_tracin(param, tracin_lines):
        """
        Parse spacer data from tracin and substitute with input_key
        """
        param['var_val'] = []

        for line_num, line in enumerate(tracin_lines):
            if 'gridid' in line:
                # ??? offset by 1 as gridid in tracin is just a comment
                card = tracin_lines[line_num+1].split()[0]
                if card == param['num']: # ??? Check if the gridid is the one
                                         # which is requested
                    # 'card' specifies which line
                    offset = 2 * int(param['var_card']) - 1
                    # 'word' specifies which element in a line
                    card = tracin_lines[line_num+offset].split()
                    # Do the substitution here
                    param['var_val'].append(card[int(param['var_word'])-1])
                    card[int(param['var_word'])-1] = '%({})s' \
                                                   .format(param['input_keys'][0])
                    tracin_lines[line_num+offset] = \
                            ''.join('%14s' % k for k in card)

        return tracin_lines

    def parse_matprop_tracin(param, tracin_lines):
        """
        Parse matprop data from tracin and substitute with input_key if any
        """
        from .shared_data import matprop_tracin_key, fit_table_keys

        param['var_val'] = []

        for line_num, line in enumerate(tracin_lines):
            # ??? check the keyword hinting a material spec. and its number
            if matprop_tracin_key in line and param['num'] == line.split()[5]:
                param['var_val'] = []
                if param['var_type'] == 'table':
                    new_lines = tracin_lines[line_num:]
                    # ??? now look which line we start substituting
                    for new_line_num, new_line in enumerate(new_lines):
                        if param['var_name'] in new_line:
                            break
                    # Now process
                    for k in range(1, int(param['var_num'])+1):
                        card = tracin_lines[line_num+new_line_num+k].split()
                        card1 = card[0:2]
                        card2 = card[2: ]
                        offset = int(param['var_col']) - 1
                        if int(param['var_col']) < 5:
                            param['var_val'].append(card2[offset])
                            card2[offset] = '%({})s' \
                                    .format(param['input_keys'][k-1])
                        else:
                            # Assign value to the dict
                            # ??? Don't take the continuation symbol
                            param['var_val'].append(card2[offset][:-1])
                            if k < int(param['var_num']):
                                card2[offset] = '%({})ss' \
                                        .format(param['input_keys'][k-1])
                            else:
                            # ??? if it's not the last element TRACE needs a
                            # continuation symbol 'e'
                                card2[offset] = '%({})se' \
                                        .format(param['input_keys'][k-1])
                        card1 = ' '.join(card1)
                        card2 =  ''.join('%14s' % val for val in card2)
                        tracin_lines[line_num+new_line_num+k] = card1 + card2

                elif param['var_type'] == 'fit':
                    new_lines = tracin_lines[line_num:]
                    for new_line_num, new_line in enumerate(new_lines):
                        if fit_table_keys[param['var_name']] in new_line:
                            break
                    card = tracin_lines[line_num+new_line_num+2].split()
                    param['var_val'].append(card[3])
                    card[3] = '%({})s' .format(param['input_keys'][0])
                    param['var_val'].append(card[4])
                    card[4] = '%({})s' .format(param['input_keys'][1])
                    tracin_lines[line_num+new_line_num+2] = \
                            ''.join('%14s' % val for val in card)
                    card = tracin_lines[line_num+new_line_num+4].split()
                    param['var_val'].append(card[0])
                    card[0] = '%({})s' .format(param['input_keys'][2])
                    param['var_val'].append(card[1])
                    card[1] = '%({})s' .format(param['input_keys'][3])
                    param['var_val'].append(card[2])
                    card[2] = '%({})s' .format(param['input_keys'][4])
                    param['var_val'].append(card[3])
                    card[3] = '%({})s' .format(param['input_keys'][5])
                    tracin_lines[line_num+new_line_num+4] = \
                                ''.join('%14s' % val for val in card)
                else:
                    print("Strange... I don't recognize any type of matprop.")
                    raise SystemExit

        return tracin_lines

    def parse_comp_tracin(param, tracin_lines):
        """
        Parse component data be it scalar or array type from tracin and
        substitute it with input_key if specified.
        """
        param['var_val'] = []
        for line_num, line in enumerate(tracin_lines):
            if line.startswith(param['type']):
            #if param['type'] in line:
                card = tracin_lines[line_num].split()[1]
                # ??? Check the component number
                if card == param['num']:
                    if param['var_type'] == 'scalar':
                        offset = 2 * int(param['var_card']) - 2
                        card = tracin_lines[line_num+offset].split()
                        param['var_val'].append(card[int(param['var_word'])-1])
                        card[int(param['var_word'])-1] = \
                                '%({})s' .format(param['input_keys'][0])
                        tracin_lines[line_num+offset] = \
                                ''.join('%14s' % val for val in card)

                    elif param['var_type'] == 'table':
                        new_lines = tracin_lines[line_num:]
                        for new_line_num, new_line in enumerate(new_lines):
                            if param['var_name'] in new_line:
                                break
                        for k in range(int(param['var_num'])):
                            # Define a filter function below
                            def f(x): return x != None and x != ''
                            card = tracin_lines[line_num+new_line_num+k]
                            card = list(filter(f, re.split("(\*)| ", card)))
                            card1 = card[0:3] # ??? The array card title
                            card2 = card[3: ]
                            offset = int(param['var_col']) - 1
                            # Assign value to the dict
                            # ??? Don't take the continuation symbol
                            param['var_val'].append(card2[offset][:-1])
                            # ??? if it's not the last element TRACE needs a
                            # continuation symbol 'e'
                            if k < int(param['var_num']) - 1:
                                card2[offset] = '%({})ss' \
                                        .format(param['input_keys'][k])
                            else:
                                card2[offset] = '%({})se' \
                                        .format(param['input_keys'][k])
                            card1 = ' '.join(card1)
                            card2 =  ''.join('%14s' % val for val in card2)
                            tracin_lines[line_num+new_line_num+k] = \
                                    card1 + card2

        return tracin_lines

    def parse_sens_coef_tracin(param, tracin_lines):
        """
        Parse sensitivity coefficient data
        """
        param['var_val'] = []
        for line_num, line in enumerate(tracin_lines):
            if line.startswith(param['num']):
                card = tracin_lines[line_num].split()
                param['var_val'].append(card[2])
                card[2] = '%({})s' .format(param['input_keys'][0])
                tracin_lines[line_num] = '{:15s} {:15s} {:15s}' .format(*card)
        return tracin_lines

    for param in input_vars:

        # process spacer
        if param['type'] == 'spacer':
            parse_spacer_tracin(param, tracin_lines)

        elif param['type'] == 'matprop':
            parse_matprop_tracin(param, tracin_lines)

        elif param['type'] in components:
            parse_comp_tracin(param, tracin_lines)

        elif param['type'] == 'senscoef':
            parse_sens_coef_tracin(param, tracin_lines)

        else:
            print("Can't recognize the parameter type")
            raise SystemExit

    with open(template_filename, 'wt') as template_file:
        for line in tracin_lines:
            print(line, file=template_file)

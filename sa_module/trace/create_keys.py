def create_keys(input_vars, keyword='$'):
    """Create and assign keys for each data type. 

    Each data type is specified in the input_vars list of dictionary.
    They will be used to create a template where each keys will be substituted
    with randomized value.

    :param input_vars: (list of dictionary) Keys will be created based on 
        their order
    :param keyword: (str, optional) if you want to create a key with more 
        verbose version
    :returns: (list of dictionary) Keys added to the as key-value pair in the 
        each entry of this list of dictionary. In other words, new 
        key has been generated.
    """
    from .shared_data import components

    for param_num, param in enumerate(input_vars):

        if param['type'] == 'spacer':
            param['input_keys'] = ['{}_{}' .format(keyword, param_num)]

        elif param['type'] == 'matprop':
            if param['var_type'] == 'table':
                num_keys = int(param['var_num'])
            elif param['var_type'] == 'fit':
                num_keys = 6

            param['input_keys'] = []
            for i in range(num_keys):
                key = '{}_{}_{}' .format(keyword, param_num, i)
                param['input_keys'].append(key)

        elif param['type'] in components:
            if param['var_type'] == 'scalar':
                param['input_keys'] = ['{}_{}' .format(keyword, param_num)]
            elif param['var_type'] == 'array' or param['var_type'] == 'table':
                param['input_keys'] = []
                num_keys = int(param['var_num'])
                for i in range(num_keys):
                    key = '{}_{}_{}' .format(keyword, param_num, i)
                    param['input_keys'].append(key)

        elif param['type'] == 'senscoef':
           param['input_keys'] = ['{}_{}' .format(keyword, param_num)]

        else:
            print("Parameter Type cannot be determined.")
            raise SystemExit

    return input_vars

def assign_keys(input_vars, sampled_array):
    """Replace the corresponding values with actual parameter values.

    Read (rescaled) sample data file and assign the values to the corresponding
    keys.

    :param input_vars: (list of dictionary) Read from input, keys has been 
        created and initial values assigned.
    :param sampled_array: (numpy array) A set of rescaled parameter values.
    :returns: (dictionary) A dictionary with keys and their corresponding value
    """
    import numpy as np

    keys_dict = {}
    for j, param in enumerate(input_vars):
        for i in range(len(param['var_val'])):
# !!! QUICK FIX for +- kind of parameter
# !!! QUICK FIX for TLIN in fill BC
# TODO make more elegant solution
            if param['type'] == 'fill' and (param['var_name'] == 'tltb' or param['var_name'] == 'tlin'):
                values = sampled_array[j] + float(param['var_val'][i])
            elif param['var_type'] == 'table' or param['var_type'] == 'fit': # Multiply values
                values = sampled_array[j] * float(param['var_val'][i])
            elif param['type'] == 'break' and param['var_name'] == 'pin':
                values = sampled_array[j] * float(param['var_val'][i])
            elif param['type'] == 'power' and param['var_name'] == 'rpowri':
                values = sampled_array[j] * float(param['var_val'][i])
            elif param['type'] == 'fill' and param['var_name'] == 'flowin':
                values = sampled_array[j] * float(param['var_val'][i])
            else: # Replace values
                values = sampled_array[j]
            #TODO Here only valid for Float parameter values (the formatting)
            # Would not work for int
            if param['var_type'] == 'fit':
                keys_dict[param['input_keys'][i]] = '{:.6e}' .format(values) # Because we are dealing with a very small number here. Well, usually.
            elif param['var_type'] == 'scalar' and param['var_name'] == 'epsw':
                keys_dict[param['input_keys'][i]] = '{:.6e}' .format(values)
            else:
                keys_dict[param['input_keys'][i]] = '{:.4f}' .format(values)

    return keys_dict

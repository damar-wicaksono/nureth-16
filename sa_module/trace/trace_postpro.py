__author__ = "WD41"

def xtv_to_csv(
    aptplot_exec,
    xtv_filename,
    trace_vars,
    case,
    sample_no,
    xtv_ext='xtv',
    workdir='.'):
    """ Extract requested variables and write them in csv format file.

    Run aptplot in batch mode to extract the supplied variable list from the
    xtv file. The output time series is written in csv format files.

    :param aptplot_exec: (str) The executable name of aptplot, should be 
        callable from $PATH.
    :param xtv_file: (str) The name of the TRACE xtv file, note that by 
        default the xtv is separated.
    :param trace_vars: (list of str) List of string of required TRACE 
        variables. The naming convention follows graphicfile for xtv.
    :param xtv_ext: (str, optional) xtv file extension by default it is 'xtv'.
    :param workdir: (str, optional) the working directory where aptplot 
        will be executed if not given then it assumes it is the current 
        working directory where the script is executed
    :returns: a CSV file named csv_file for all variable listed in trace_vars
    """
    import subprocess
    import os

    csv_filename = '{}.csv' .format(xtv_filename)
    xtv_filename = '{}.{}' .format(xtv_filename, xtv_ext)
    apt_script_filename = workdir + '/tmp.apt'

    # Create aptplot batch script to extract variables
    with open(apt_script_filename, 'w') as apt_script:
        apt_script.write('TRAC 0 XTV "{}" \n' .format(xtv_filename))
        for trace_var in trace_vars:
            apt_script.write('TREAD 0 "{}" SIU \n' .format(trace_var))
        apt_script.write('TRAC 0 EXPORT CSV "{}" \n' .format(csv_filename))
        apt_script.write('TRAC 0 CLOSE \n')
        apt_script.write('EXIT \n')

    # Execute and clean up files
    try:
        scr_filename = workdir + '/tmp.scr'
        with open(scr_filename, 'w') as scr_file:
            apt_run = subprocess.Popen(
                [aptplot_exec, '-batch', 'tmp.apt', '-nowin'],
                stdout = scr_file,
                cwd=workdir
            )
            apt_run.wait()
    except subprocess.CalledProcessError:
        print('Postprocessing of xtv file failed.')
    else:
        print('Postprocessing of xtv file from Test No. {} sample No. {} '\
            'is complete.' .format(case, sample_no))

    os.remove(apt_script_filename)
    os.remove(scr_filename)


# -*- coding: utf-8 -*-
"""Module containing Hedge fund portfolio test function"""
__author__ = "WD41"

import numpy as np


def evaluate(p, c):
    r"""Evaluate the estimated risk
    
    :math:`y = C_1 \cdot P_1 + C_2 \cdot P_2 + C_3 \cdot P_3`
    
    :param p: (ndarray) A vector containing the hedged portfolios
    :param c: (ndarray) A vector containing the number of portfolios
    :returns : (float) The estimated risk in euro
    """
    return np.sum(p * c)
    
def evaluate_abs(p, c):
    """Evaluate the absolute estimated risk
    
    :param p: (ndarray) A vector containing the hedged portfolios
    :param c: (ndarray) A vector containing the number of portfolios
    :returns : (float) The estimated risk in euro
    """
    return abs(np.sum(p * c))
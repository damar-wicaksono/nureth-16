# -*- coding:utf-8 -*-
"""Module containing the Sobol G test function"""
__author__ = "WD41"

import numpy as np


def evaluate(x, a):
    r"""Evaluate the Sobol G test function
    
    :math:`f(x) = \Gamma_i^n g_i(x_i) = 4 \times x_i - 2 + a_i / 1 + a_i`
    
    :param x: (ndarray) A vector of parameter values
    :param a: (ndarray) A vector of `a` values
    :returns: (float) The function scalar evaluated value
    """
    sobol_g = (abs(4 * x - 2) + a) / (1 + a) 
    
    return np.prod(sobol_g)
    
def calc_a(n):
    """Calculate the vector of `a` used in the Sobol-G function
    
    :param n: (int) The number of parameters
    :returns: (ndarray) The vector containing `a`
    """
    a = np.empty([n])
    
    for i in range(n):
        a[i] = i / 2
    
    return a
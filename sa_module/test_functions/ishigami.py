# -*- coding: utf-8 -*-
"""Module containing the Ishigami test function"""
__author__ = "WD41"

import numpy as np


def evaluate(x, coefs):
    r"""Evaluate the Ishigami function 

    Ishigami function is a scalar value, three-variables function.
    
    :math:`f(x) = sin(x1) + a \cdot sin^2(x2) + b \cdot x3^4 \cdot sin(x1)`
    
    :param x: (ndarray) A vector of the function parameters
    :param coefs: (ndarray) A vector of the function coefficients
    :returns: (float) The scalar function value
    """
    ishigami = np.sin(x[0]) + \
        coefs[0] * np.sin(x[1])**2 + \
        coefs[1] * x[2]**4 * np.sin(x[0])

    return ishigami

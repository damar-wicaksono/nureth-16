# postpro-R

Collection of R scripts to carry out functional data analysis on the raw 
time-dependent temperature output (at mid assembly) and produce post-processed
data used as inputs to the Sobol' indices computation routines (`postpro-py`).

The functional data analysis was done in 3 main steps:
 1. Create smoothed functional data object
 2. Compute landmarks and carry out landmark registration to exclude phase 
    variation 
 3. Carry out principal component analysis on registered reflood curves

The scripts include:

 - `postpro_R.R`: the driver script
 - `sobol_tc_smooth.R`: script to create functional data object
 - `sobol_tc_landmarks.R`: script to compute the landmarks of the reflood curve 
 - `sobol_tc_register.R`: script to register the reflood curve based on the 
   landmark
 - `sobol_tc_pca.R`: script to carry out principal component analysis on the 
   registered reflood curve and write down the scores into file
 - `sobol_tc_regfd2csv.R`: script to write down the registered reflood curves
   into set of `.csv` files 

All the computation can be carried out by running the driver script (all the 
required packages are resolved by the `packrat` package) `postpro-R.R` in the 
terminal:

    Rscript postpro-R.R

The pre-requisite files and the outputs produced by running all the scripts 
can be found in the in-code documentation. The outputs are kept in the 
`mid-2000` folder. Note that running the script is time demanding.

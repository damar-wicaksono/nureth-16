#
# title     : fda_regress.R
# purpose   : Create B-spline basis functions representation for TRACE output 
#             of FEBA simulation result
# author    : WD41, LRS/EPFL/PSI
# date      : June 2014
#

# Create Data Matrix of all runs ----------------------------------------------
# "fda" package works nicely if the replications are put in a Matrix form

# Specify the time grid
delta_t <- 0.10
t_start <- datamat_raw[, 1][0]
t_end <- datamat_raw[, 1][length(datamat_raw[, 1])]
unif_time <- seq(0, t_end, delta_t)

# Create Data Matrix of the Base Case Run
datamat <- matrix(datamat_raw[, 2], length(unif_time), N_replications + 1)
names <- rep(NA, N_replications + 1)
names[1] <- "base"

# Loop over all replications and fill in the matrix
for (i in 2:(N_replications + 1)) {
    names[i] <- paste("run-", i-1, sep="")
    datamat[,i] <- datamat_raw[, i+1]
    print(
        paste(
            "Constructing uniform data matrix... at replication number = ", 
            i-1)
    )
}

# Put the row names and column names 
colnames(datamat) <- names
rownames(datamat) <- paste(as.numeric(unif_time))

# Represent the data in the B-spline basis function via regression ------------

# Specify the knots location
j_th <- 4    # Take every 4th point
n_order <- 4  # The order of basis function (order = degree + 1)
w_knots <- unif_time[seq(1, length(unif_time) - 1, by = j_th)]
w_knots <- append(w_knots, unif_time[length(unif_time)])
# Specify the number of basis functions
n_basis <- length(w_knots) + n_order - 2

# Construct the basis function
tc_basis  <- create.bspline.basis(
    c(unif_time[0], unif_time[length(unif_time)]), 
    n_basis, 
    n_order, 
    w_knots
)

# Construct the regression parameters
Lfdobj    <- 2             # penalize curvature of acceleration, 
# but not really as the lambda is set to zero
lambda    <- 0.0           # zero smoothing parameter
cvecf     <- matrix(0, n_basis, 1)
Wfd0      <- fd(cvecf, tc_basis)
tc_fd_params <- fdPar(Wfd0, Lfdobj, lambda)

# Create the basis function representation of all the replications
tc_smooth <- smooth.basis(unif_time, datamat, tc_fd_params)
#
# title     : sobol_tc_landmarks.R
# purpose   : Compute the time of max. temperature and the time of quenching of 
#           : all the realizations
# author    : WD41, LRS/EPFL/PSI
# date      : March 2015
#

# Read the data ---------------------------------------------------------------
tc_smooth <- readRDS(inpfullname)
unif_time <- as.numeric(tc_smooth$fd$fdnames$time)
# Number of replications is minus 1 (excluding the base)
N_replications <- length(tc_smooth$fd$fdnames$reps) - 1

# Compute the timing of important landmarks -----------------------------------
source("./Rscripts/fda_landmarks.R")

# Save the results into files -------------------------------------------------
# these are texts file (technically its .csv)
write.table(landmark_replicates, otpfullnames[1], sep = ",") # the landmarks
write.table(landmark_target, otpfullnames[2], sep = ",")     # the target

